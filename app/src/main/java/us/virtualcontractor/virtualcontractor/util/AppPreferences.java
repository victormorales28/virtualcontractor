package us.virtualcontractor.virtualcontractor.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by ucweb02 on 20/05/2015.
 */
public class AppPreferences {

    private SharedPreferences shrPref;
    private Context context;
    private SharedPreferences.Editor editorShrPref;

    public AppPreferences(Context ctx, String namePreferences) {
        this.context = ctx;
        shrPref = ctx.getSharedPreferences(namePreferences, Context.MODE_PRIVATE);
        editorShrPref = shrPref.edit();
    }

    public void setPreferences(String key, String value){
        editorShrPref.putString(key, value);
        editorShrPref.commit();
    }

    public String getPreferences(String key, String defValue){
        return shrPref.getString(key, defValue);
    }

}
