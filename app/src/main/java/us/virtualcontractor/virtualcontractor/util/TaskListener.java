package us.virtualcontractor.virtualcontractor.util;

/**
 * Created by ucweb01 on 11/05/2015.
 */
public interface TaskListener {
    void onTaskStarted();

    void onTaskFinished(String result);
}
