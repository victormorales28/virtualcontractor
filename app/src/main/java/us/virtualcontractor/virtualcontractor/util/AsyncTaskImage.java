package us.virtualcontractor.virtualcontractor.util;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import us.virtualcontractor.virtualcontractor.interfaces.TaskImageListener;

/**
 * Created by ucweb02 on 18/05/2015.
 */
public class AsyncTaskImage extends AsyncTask<ArrayList<String>, Void, ArrayList<String>>{

    private final TaskImageListener taskListener;
    private ProgressDialog progressDialog = null;
    private ArrayList<String> imgsPath;
    private Bitmap bitmap;

    public AsyncTaskImage(TaskImageListener taskListener) {
        this.taskListener = taskListener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (progressDialog != null)
            progressDialog.setMessage("Uploading avatar...");
    }

    @Override
    protected ArrayList<String> doInBackground(ArrayList<String>... params) {
        ArrayList<String> encodeImageString = new ArrayList<String>();
//        BitmapFactory.Options options = new BitmapFactory.Options();
//        options.inSampleSize = 3;
        imgsPath = params[0];
        for (int i = 0; i < imgsPath.size(); i++) {
//            bitmap = BitmapFactory.decodeFile(imgsPath.get(i),options);
            bitmap = decodeSampledBitmapFromFile(imgsPath.get(i), 250, 250);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            // Must compress the Image to reduce image size to make upload easy
            bitmap.compress(Bitmap.CompressFormat.PNG, 50, stream);
            byte[] byte_arr = stream.toByteArray();
            // Encode Image to String
            encodeImageString.add(Base64.encodeToString(byte_arr, 0));
            bitmap = null;
        }
        return encodeImageString;
    }

    @Override
    protected void onPostExecute(ArrayList<String> strings) {
//        super.onPostExecute(strings);
        if (progressDialog != null)
            progressDialog.setMessage("Image conpress...");
        taskListener.onAsyncTaskImageFinished(strings);
    }

    public void setProgressDialog(ProgressDialog prgs){
        this.progressDialog = prgs;
    }

    public static Bitmap decodeSampledBitmapFromFile(String filename, int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filename, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(filename, options);
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }
}
