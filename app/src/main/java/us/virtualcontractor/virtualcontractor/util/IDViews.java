package us.virtualcontractor.virtualcontractor.util;

import us.virtualcontractor.virtualcontractor.R;

/**
 * Created by ucweb02 on 22/05/2015.
 */
public class IDViews {

    public static final int[] ID_VALIDATE_CLIENT = new int[]{
            R.id.txtFirstName,                  // index: 0
            R.id.txtLastName,                   // index: 1
            R.id.txtTelephone,                  // index: 2
            R.id.txtPhone,                      // index: 3
            R.id.txtState,                      // index: 4
            R.id.txtCity,                       // index: 5
            R.id.txtEmail,                      // index: 6
            R.id.txtPassword,                   // index: 7
            R.id.txtConfirmPassword};           // index: 8

    public static final int[] ID_VALIDATE_CONTRACTOR = new int[]{
            R.id.txtFirstName,                  // index: 0
            R.id.txtLastName,                   // index: 1
            R.id.txtCompany,                    // index: 2
            R.id.txtTelephone,                  // index: 3
            R.id.txtPhone,                      // index: 4
            R.id.txtState,                      // index: 5
            R.id.txtCity,                       // index: 6
            R.id.txtProfession,                 // index: 7
            R.id.txtEmail,                      // index: 8
            R.id.txtPassword,                   // index: 9
            R.id.txtConfirmPassword};           // index: 10

    public static final int[] ID_VALIDATE_SEARCH_CONTRACTOR = new int[]{
            R.id.txt_all_profession,            // index: 0
            R.id.txtCiudad};                    // index: 1

    public static final String[] MSG_ERROR_VALIDATE_CLIENT = new String[]{
            "First Name is required!",          // index: 0
            "Last Name is required!",           // index: 1
            "Telephone is required!",           // index: 2
            "Phone is required!",               // index: 3
            "State is required!",               // index: 4
            "City is required!",                // index: 5
            "Email is required!",               // index: 6
            "Password is required!",            // index: 7
            "Password Confirm is required!"};   // index: 8

    public static final String[] MSG_ERROR_VALIDATE_CONTRACTOR = new String[]{
            "First Name is required!",          // index: 0
            "Last Name is required!",           // index: 1
            "Company Name is required!",        // index: 2
            "Telephone is required!",           // index: 3
            "Phone is required!",               // index: 4
            "State is required!",               // index: 5
            "City is required!",                // index: 6
            "Profession is required!",          // index: 7
            "Email is required!",               // index: 8
            "Password is required!",            // index: 9
            "Password Confirm is required!"};   // index: 10

    public static final String[] MSG_VALIDATE_SEARCH_CONTRACTOR = new String[]{
            "Select at least one profession!",
            "City is required!"
    };
}
