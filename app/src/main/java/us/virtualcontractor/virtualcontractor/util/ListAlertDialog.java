package us.virtualcontractor.virtualcontractor.util;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by ucweb02 on 17/04/2015.
 */
public class ListAlertDialog {

    public ArrayList<String> listaRegistrarComo = new ArrayList<String>(Arrays.asList("Constructor", "Client"));

    public ArrayList<String> listaEstados = new ArrayList<String>(Arrays.asList(
            "Alabama",
            "Alaska",
            "Arizona",
            "Arkansas",
            "California",
            "Carolina del Norte",
            "Carolina del Sur",
            "Colorado",
            "Connecticut",
            "Dakota del Norte",
            "Dakota del Sur",
            "Delaware",
            "Florida",
            "Georgia",
            "Hawái",
            "Idaho",
            "Illinois",
            "Indiana",
            "Iowa",
            "Kansas",
            "Kentucky",
            "Luisiana",
            "Maine",
            "Maryland",
            "Massachusetts",
            "Míchigan",
            "Minnesota",
            "Misisipi",
            "Misuri",
            "Montana",
            "Nebraska",
            "Nevada",
            "Nueva Jersey",
            "Nueva York",
            "Nuevo Hampshire",
            "Nuevo México",
            "Ohio",
            "Oklahoma",
            "Oregón",
            "Pensilvania",
            "Rhode Island",
            "Tennessee",
            "Texas",
            "Utah",
            "Vermont",
            "Virginia",
            "Virginia Occidental",
            "Washington",
            "Wisconsin",
            "Wyoming"));

    public ArrayList<ArrayList<String>> listaCiudades = new ArrayList<ArrayList<String>>();

    public ArrayList<String> listaProfesiones = new ArrayList<String>(Arrays.asList("Profesion 1", "Profesion 2", "Profesion 3", "Profesion 4", "Profesion 5"));

    public ListAlertDialog(){
        ArrayList<String> lCiudades = new ArrayList<String>(Arrays.asList("City 1 -> 1", "City 1 -> 2", "City 1 -> 3"));
        listaCiudades.add(lCiudades);
        lCiudades = new ArrayList<String>(Arrays.asList("City 2 -> 1", "City 2 -> 2", "City 2 -> 3"));
        listaCiudades.add(lCiudades);
        lCiudades = new ArrayList<String>(Arrays.asList("City 3 -> 1", "City 3 -> 2", "City 3 -> 3"));
        listaCiudades.add(lCiudades);
        lCiudades = new ArrayList<String>(Arrays.asList("City 4 -> 1", "City 4 -> 2", "City 4 -> 3"));
        listaCiudades.add(lCiudades);
        lCiudades = new ArrayList<String>(Arrays.asList("City 5 -> 1", "City 5 -> 2", "City 5 -> 3"));
        listaCiudades.add(lCiudades);
        lCiudades = new ArrayList<String>(Arrays.asList("City 6 -> 1", "City 6 -> 2", "City 6 -> 3"));
        listaCiudades.add(lCiudades);
        lCiudades = new ArrayList<String>(Arrays.asList("City 7 -> 1", "City 7 -> 2", "City 7 -> 3"));
        listaCiudades.add(lCiudades);
        lCiudades = new ArrayList<String>(Arrays.asList("City 8 -> 1", "City 8 -> 2", "City 8 -> 3"));
        listaCiudades.add(lCiudades);
        lCiudades = new ArrayList<String>(Arrays.asList("City 9 -> 1", "City 9 -> 2", "City 9 -> 3"));
        listaCiudades.add(lCiudades);
        lCiudades = new ArrayList<String>(Arrays.asList("City 10 -> 1", "City 10 -> 2", "City 10 -> 3"));
        listaCiudades.add(lCiudades);
    }

}
