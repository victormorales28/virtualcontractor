package us.virtualcontractor.virtualcontractor.db;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import us.virtualcontractor.virtualcontractor.model.MCiudad;
import us.virtualcontractor.virtualcontractor.model.MContratista;
import us.virtualcontractor.virtualcontractor.model.MEstado;
import us.virtualcontractor.virtualcontractor.model.MLocation;
import us.virtualcontractor.virtualcontractor.model.MPerfilContratista;
import us.virtualcontractor.virtualcontractor.model.MPerfilProyecto;
import us.virtualcontractor.virtualcontractor.model.MPerfilSemana;
import us.virtualcontractor.virtualcontractor.model.MPerfilUsuario;
import us.virtualcontractor.virtualcontractor.model.MProfesion;
import us.virtualcontractor.virtualcontractor.model.MProfesiones;
import us.virtualcontractor.virtualcontractor.model.MUsuario;

/**
 * Created by ucweb01 on 06/05/2015.
 */
public class consult {
    conexion_ws oConexion_ws;

    public consult() {
        this.oConexion_ws = new conexion_ws();
    }

    public MLocation getEstadoCiudad(){

        MEstado oMEstado;
        ArrayList<MEstado> lEstados = new ArrayList<MEstado>();

        MCiudad oMCiudad;
        ArrayList<MCiudad> lCiudades = new ArrayList<MCiudad>();

        MLocation mLoc = new MLocation(lEstados, lCiudades);
        String data = oConexion_ws.getData("getEstadoCiudad.php",new String[]{});
        if(!data.equalsIgnoreCase("")){
            JSONObject json;
            try {
                json = new JSONObject(data);

                JSONArray jAEstado = json.optJSONArray("estado");
                for (int i = 0; i < jAEstado.length(); i++) {
                    oMEstado = new MEstado();
                    JSONObject jsonArrayChild = jAEstado.getJSONObject(i);
                    oMEstado.setId_estado(Integer.parseInt(jsonArrayChild.optString("id_estado")));
                    oMEstado.setNombre_estado(jsonArrayChild.optString("nombre_estado"));
                    lEstados.add(oMEstado);
                }

                String id_estado = "0";
                JSONArray jACiudad = json.optJSONArray("ciudad");
                for (int i = 0; i < jACiudad.length(); i++) {
                    oMCiudad = new MCiudad();
                    JSONObject jsonArrayChild = jACiudad.getJSONObject(i);
                    oMCiudad.setId_ciudad(Integer.parseInt(jsonArrayChild.optString("id_ciudad")));
                    oMCiudad.setNombre_ciudad(jsonArrayChild.optString("nombre_ciudad"));
                    oMCiudad.setId_estado(Integer.parseInt(jsonArrayChild.optString("id_estado")));
                    lCiudades.add(oMCiudad);
                }

                mLoc = new MLocation(lEstados, lCiudades);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return mLoc;
    }

    public MUsuario login(String nombreusuario, String contrasenia){
        MUsuario mu = null;
        String data = oConexion_ws.getData("login.php", new String[]{nombreusuario, contrasenia});
        if(!data.equalsIgnoreCase("")){
            JSONObject json;
            try {
                json = new JSONObject(data);
                JSONArray jAEstado = json.optJSONArray("usuario");
                JSONObject jsonArrayChild = jAEstado.getJSONObject(0);
                mu = new MUsuario(jsonArrayChild.optInt("id_usuario"), nombreusuario, contrasenia, jsonArrayChild.optInt("tipo_usuario"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return mu;
    }

    public ArrayList<MContratista> getMisContractistas(String url, String[] args){

        MContratista oMContratista;
        ArrayList<MContratista> lContratista = new ArrayList<MContratista>();
        String data = oConexion_ws.getData(url, args);
        if(!data.equalsIgnoreCase("")){
            JSONObject json;
            try {
                json = new JSONObject(data);
                JSONArray jAContratista = json.optJSONArray("contratistas");
                for (int i = 0; i < jAContratista.length(); i++){
                    JSONObject jsonArrayChild = jAContratista.getJSONObject(i);
                    oMContratista = new MContratista("",0,"","",0);
                    oMContratista.setNombre(jsonArrayChild.optString("nombres_contratista"));
                    oMContratista.setApellido(jsonArrayChild.optString("apellidos_contratista"));
                    oMContratista.setCompania(jsonArrayChild.optString("compania_contratista"));
                    oMContratista.setTelefono(jsonArrayChild.optString("telefono_contratista"));
                    oMContratista.setMovil(jsonArrayChild.optString("movil_contratista"));
                    oMContratista.setEmail(jsonArrayChild.optString("email_contratista"));
                    oMContratista.setAvatar(jsonArrayChild.optString("avatar_contratista"));
                    oMContratista.setNombre_estado(jsonArrayChild.optString("nombre_estado"));
                    oMContratista.setNombre_ciudad(jsonArrayChild.optString("nombre_ciudad"));
                    oMContratista.setId(Integer.parseInt(jsonArrayChild.optString("id_contratista")));
                    oMContratista.setProfesion(jsonArrayChild.optString("nombre_profesion"));
                    lContratista.add(oMContratista);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return lContratista;

    }

    public ArrayList<MPerfilContratista> perfilContractistas(String id){

        MPerfilContratista oMPerfilContratista;
        ArrayList<MPerfilContratista> lPerfilContratista = new ArrayList<MPerfilContratista>();
        String data = oConexion_ws.getData("perfilContratista.php",new String[]{id});

        if(!data.equalsIgnoreCase("")){
            JSONObject json;
            try {
                json = new JSONObject(data);
                JSONArray jAContratista = json.optJSONArray("contratista");
                JSONObject jsonArrayChild = jAContratista.getJSONObject(0);
                oMPerfilContratista = new MPerfilContratista(Integer.parseInt(jsonArrayChild.optString("id_contratista")),jsonArrayChild.optString("nombres_contratista"),jsonArrayChild.optString("apellidos_contratista"),jsonArrayChild.optString("compania_contratista"),jsonArrayChild.optString("telefono_contratista"),jsonArrayChild.optString("movil_contratista"),jsonArrayChild.optString("email_contratista"),jsonArrayChild.optString("avatar_contratista"),jsonArrayChild.optString("nombre_ciudad"),jsonArrayChild.optString("nombre_estado"),jsonArrayChild.optString("nombre_profesion"));
                lPerfilContratista.add(oMPerfilContratista);

                JSONArray jAProyectos = json.optJSONArray("proyectos");
                if (jAProyectos != null){
                    for (int i = 0; i < jAProyectos.length(); i++){
                        JSONObject jsonArrayChild_ = jAProyectos.getJSONObject(i);
                        oMPerfilContratista = new MPerfilContratista(jsonArrayChild_.optString("id_proyecto"),jsonArrayChild_.optString("nombre_proyecto"),jsonArrayChild_.optString("sub_titulo_proyecto"),Double.parseDouble(jsonArrayChild_.optString("valoracion")),jsonArrayChild_.optString("imagen_foto_semana"));
                        lPerfilContratista.add(oMPerfilContratista);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return lPerfilContratista;
    }

    public ArrayList<MPerfilContratista> getMyProyectos(String id, String tipo_usuario){

        MPerfilContratista oMPerfilContratista;
        ArrayList<MPerfilContratista> lPerfilContratista = new ArrayList<MPerfilContratista>();
        String data = oConexion_ws.getData("getMyProyectos.php",new String[]{id,tipo_usuario});

        if(!data.equalsIgnoreCase("")){
            JSONObject json;
            try {
                json = new JSONObject(data);
                JSONArray jAProyectos = json.optJSONArray("proyectos");
                if (jAProyectos != null){
                    for (int i = 0; i < jAProyectos.length(); i++){
                        JSONObject jsonArrayChild_ = jAProyectos.getJSONObject(i);
                        oMPerfilContratista = new MPerfilContratista(jsonArrayChild_.optString("id_proyecto"),jsonArrayChild_.optString("nombre_proyecto"),jsonArrayChild_.optString("sub_titulo_proyecto"),Double.parseDouble(jsonArrayChild_.optString("valoracion")),jsonArrayChild_.optString("imagen_foto_semana"));
                        lPerfilContratista.add(oMPerfilContratista);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return lPerfilContratista;
    }

    public ArrayList<MPerfilProyecto> perfilProyecto(String id_proyecto){

        MPerfilProyecto oMPerfilProyecto;
        ArrayList<MPerfilProyecto> lPerfilProyecto = new ArrayList<MPerfilProyecto>();

        String data = oConexion_ws.getData("perfilProyecto.php",new String[]{id_proyecto});

        if(!data.equalsIgnoreCase("")){
            JSONObject json;
            try {
                json = new JSONObject(data);
                JSONArray jAProyecto = json.optJSONArray("proyecto");
                JSONArray jASemanas = json.optJSONArray("semanas");

                JSONObject jsonArrayChild = jAProyecto.getJSONObject(0);
                oMPerfilProyecto = new MPerfilProyecto(jsonArrayChild.optString("nombre_proyecto"),jsonArrayChild.optString("sub_titulo_proyecto"),jsonArrayChild.optString("descripcion_proyecto"),jASemanas.length(),jsonArrayChild.optString("fecha_inicio_proyecto"));
                lPerfilProyecto.add(oMPerfilProyecto);

                if (jASemanas != null){
                    for (int i = 0; i < jASemanas.length(); i++){
                        JSONObject jsonArrayChild_ = jASemanas.getJSONObject(i);
                        oMPerfilProyecto = new MPerfilProyecto(jsonArrayChild_.optString("id_semana"),jsonArrayChild_.optString("numero_semana"));
                        lPerfilProyecto.add(oMPerfilProyecto);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return lPerfilProyecto;
    }

    public MPerfilProyecto perfilProyectoPrimeraSemana(String id_proyecto){

        MPerfilProyecto oMPerfilProyecto = new MPerfilProyecto("","","",0,"");
        String data = oConexion_ws.getData("perfilProyectoPrimeraSemana.php",new String[]{id_proyecto});
        if(!data.equalsIgnoreCase("")){
            JSONObject json;
            try {
                json = new JSONObject(data);
                JSONArray jAProyecto = json.optJSONArray("proyecto");
                JSONArray jAFotoSemana = json.optJSONArray("foto_semana");

                JSONObject jsonArrayChild = jAProyecto.getJSONObject(0);
                oMPerfilProyecto = new MPerfilProyecto(jsonArrayChild.optString("nombre_proyecto"),jsonArrayChild.optString("sub_titulo_proyecto"),jsonArrayChild.optString("descripcion_proyecto"),Integer.parseInt(jsonArrayChild.optString("total_semanas")),jsonArrayChild.optString("fecha_inicio_proyecto"));

                if (jAFotoSemana != null){
                    for (int i = 0; i < jAFotoSemana.length(); i++){
                        JSONObject jsonArrayChild_ = jAFotoSemana.getJSONObject(i);
                        oMPerfilProyecto.setFoto(jsonArrayChild_.optString("imagen_foto_semana"));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return oMPerfilProyecto;
    }

    public MPerfilSemana perfilSemana(String id_semana){

        MPerfilSemana oMPerfilSemana = new MPerfilSemana("","","");
        String data = oConexion_ws.getData("perfilSemana.php",new String[]{id_semana});

        if(!data.equalsIgnoreCase("")){
            JSONObject json;
            try {
                json = new JSONObject(data);
                JSONArray jASemana = json.optJSONArray("semana");
                JSONObject jsonArrayChild = jASemana.getJSONObject(0);
                oMPerfilSemana = new MPerfilSemana(jsonArrayChild.optString("titulo_semana"),jsonArrayChild.optString("avance_semana"),jsonArrayChild.optString("video_semana"));

                JSONArray jAFotos = json.optJSONArray("fotos");
                if (jAFotos != null){
                    for (int i = 0; i < jAFotos.length(); i++){
                        JSONObject jsonArrayChild_ = jAFotos.getJSONObject(i);
                        oMPerfilSemana.setFoto(jsonArrayChild_.optString("imagen_foto_semana"));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return oMPerfilSemana;
    }

    public String agregarContratista(String id_usuario, String id_contratista){

        String msj = "";
        String data = oConexion_ws.getData("AgregarContratista.php",new String[]{id_usuario,id_contratista});
        if(!data.equalsIgnoreCase("")){
            JSONObject json;
            try {
                json = new JSONObject(data);
                JSONArray jRespuesta = json.optJSONArray("respuesta");
                JSONObject jsonArrayChild = jRespuesta.getJSONObject(0);
                msj = jsonArrayChild.optString("mensaje");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return msj;
    }

    public MProfesiones getProfesiones(){
        MProfesiones omp = null;
        ArrayList<MProfesion> lmp = new ArrayList<MProfesion>();
        MProfesion mp = null;
        String data = oConexion_ws.getData("getProfesiones.php", new String[]{});
        if(!data.equalsIgnoreCase("")){
            JSONObject json;
            try {
                json = new JSONObject(data);
                JSONArray jAEstado = json.optJSONArray("profesiones");
                for (int i = 0; i < jAEstado.length(); i++) {
                    JSONObject jsonArrayChild = jAEstado.getJSONObject(i);
                    mp = new MProfesion(jsonArrayChild.optInt("id_profesion"), jsonArrayChild.optString("nombre_profesion"));
                    lmp.add(mp);
                }
                omp = new MProfesiones(lmp);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return omp;
    }

    public MPerfilUsuario getPerfilUsuario(String idUsuario, String tipoUsuario){
        MPerfilUsuario objPU = new MPerfilUsuario();
        ArrayList<Integer> profesiones = new ArrayList<Integer>();
        String data = oConexion_ws.getData("consultUser.php", new String[]{idUsuario, tipoUsuario});
        if (!data.equalsIgnoreCase("")) {
            JSONObject json;
            try {
                json = new JSONObject(data);
                JSONArray jProfileUser = json.optJSONArray("profileUser");
                JSONObject jsonArrayChild = jProfileUser.getJSONObject(0);
                objPU.setNombre(jsonArrayChild.optString("nombres"));
                objPU.setApellido(jsonArrayChild.optString("apellidos"));
                objPU.setCompania(jsonArrayChild.optString("compania"));
                objPU.setTelefono(jsonArrayChild.optString("telefono"));
                objPU.setMovil(jsonArrayChild.optString("movil"));
                objPU.setId_estado(jsonArrayChild.optInt("id_estado"));
                objPU.setId_ciudad(jsonArrayChild.optInt("id_ciudad"));
                objPU.setEmail(jsonArrayChild.optString("email"));
                objPU.setAvatar(jsonArrayChild.optString("avatar"));

                JSONArray jProfessionUser = json.optJSONArray("profession");
                for (int i = 0; i < jProfessionUser.length(); i++) {
                    JSONObject jProf = jProfessionUser.getJSONObject(i);
                    profesiones.add(jProf.getInt("id_profesion"));
                }
                objPU.setProfesion(profesiones);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return objPU;
    }
}






















