package us.virtualcontractor.virtualcontractor.db;

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;

import us.virtualcontractor.virtualcontractor.interfaces.ServerConnectionListener;

/**
 * Created by ucweb02 on 18/05/2015.
 */
public class ConnectServer {

    private RequestParams params;
    private ProgressDialog progressDialog;
    private Context context;
    private final String urlServer = "http://virtualc.uc-web.mobi/web_service/";
    private String nameService = "";
    private AsyncHttpClient httpClient;
    private ServerConnectionListener scListener;

    public ConnectServer(Context context, String nameService) {
        this.context = context;
        this.nameService = nameService;
        params = new RequestParams();
    }

    public void handleEvents(ServerConnectionListener ref){
        this.scListener = ref;
    }

    public void Upload(){
        httpClient = new AsyncHttpClient();
        progressDialog.setMessage("Validating data...");
        httpClient.post(urlServer + nameService,
            params, new AsyncHttpResponseHandler() {
                // When the response returned by REST has Http
                // response code '200'
                @Override
                public void onSuccess(String response) {
                    // Hide Progress Dialog
                    progressDialog.dismiss();
                    scListener.serverConnectionOnSuccess(response);
                }

                // When the response returned by REST has Http
                // response code other than '200' such as '404',
                // '500' or '403' etc
                @Override
                public void onFailure(int statusCode, Throwable error, String content) {
                    // Hide Progress Dialog
                    progressDialog.dismiss();
                    // When Http response code is '404'
                    if (statusCode == 404) {
                        Toast.makeText(context, "Requested resource not found", Toast.LENGTH_LONG).show();
                    }
                    // When Http response code is '500'
                    else if (statusCode == 500) {
                        Toast.makeText(context, "Something went wrong at server end", Toast.LENGTH_LONG).show();
                    }
                    // When Http response code other than 404, 500
                    else {
                        Toast.makeText(context,
                                "Error Occured \n Most Common Error: \n1. Device not connected to Internet\n2. Web App is not deployed in App server\n3. App server is not running\n HTTP Status code : "
                                        + statusCode, Toast.LENGTH_LONG).show();
                    }
                }
            });
    }

    public void addParameter(String name, String value){
        params.add(name, value);
    }

    public void setProgressDialog(ProgressDialog prgs){
        this.progressDialog = prgs;
    }

}

