package us.virtualcontractor.virtualcontractor.db;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ucweb01 on 06/05/2015.
 */
public class conexion_ws {

    public String getData(String url, String[] args){
        List<NameValuePair> nameValuePairs;
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://virtualc.uc-web.mobi/web_service/" + url);
        String result="";
        HttpResponse response;
        try {
            if(args.length > 0){
                nameValuePairs = new ArrayList<NameValuePair>(args.length);
                for (int i=0; i< args.length ;i++){
                    nameValuePairs.add(new BasicNameValuePair("param_"+Integer.toString(i),args[i].toString()));
                }
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            }
            response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            InputStream inStream = entity.getContent();
            result = convertStreamToString(inStream);
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private String convertStreamToString(InputStream is) throws IOException {
        if (is != null) {
            StringBuilder sb = new StringBuilder();
            String line;
            try {
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(is, "UTF-8"));
                while ((line = reader.readLine()) != null) {
                    sb.append(line).append("\n");
                }
            }
            finally {
                is.close();
            }
            return sb.toString();
        } else {
            return "";
        }
    }
}
