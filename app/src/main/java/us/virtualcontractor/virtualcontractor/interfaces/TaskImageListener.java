package us.virtualcontractor.virtualcontractor.interfaces;

import java.util.ArrayList;

/**
 * Created by ucweb02 on 18/05/2015.
 */
public interface TaskImageListener {
//    void onAsyncTaskImageStarted();

    void onAsyncTaskImageFinished(ArrayList<String> strings);
}