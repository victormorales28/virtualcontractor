package us.virtualcontractor.virtualcontractor.interfaces;

/**
 * Created by ucweb02 on 06/05/2015.
 */
public interface EventCustomAlertDialog {
    void onADSelectedItem(int index);
    void onADClickButtonOk();
}
