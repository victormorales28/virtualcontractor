package us.virtualcontractor.virtualcontractor;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import us.virtualcontractor.virtualcontractor.db.consult;
import us.virtualcontractor.virtualcontractor.model.MPerfilProyecto;
import us.virtualcontractor.virtualcontractor.model.MPerfilSemana;
import us.virtualcontractor.virtualcontractor.viewPager.DepthPageTransformer;
import us.virtualcontractor.virtualcontractor.viewPager.MyFragmentPagerAdapter;
import us.virtualcontractor.virtualcontractor.viewPager.ViewPagerFragmentProyect;


public class detailProject extends ActionBarActivity {

    private ViewPager pager = null;
    private TextView txtNombre;
    private TextView totalSemanas;
    private TextView fechaInicio;
    private consult oConsult;

    private String id_semana;
    private static MPerfilSemana oMPerfilSemana = new MPerfilSemana("","","");

    private TextView numberWeek;
    private TextView titulo;
    private TextView avance;

    private static MPerfilProyecto oMPerfilProyecto = new MPerfilProyecto("","","",0,"");
    private String id_proyecto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_project);

        numberWeek = (TextView) findViewById(R.id.numberWeek);
        titulo = (TextView) findViewById(R.id.titulo);
        avance = (TextView) findViewById(R.id.avance);

        Bundle bundle = getIntent().getExtras();
        pager = (ViewPager) this.findViewById(R.id.pager);

        if(bundle.getString("tipo").equals("semana")){

            LinearLayout lyProjectName = (LinearLayout) findViewById(R.id.lyProjectName);
            lyProjectName.setVisibility(View.GONE);

            LinearLayout lyDate = (LinearLayout) findViewById(R.id.lyDate);
            lyDate.setVisibility(View.GONE);

            numberWeek.setText("WEEK " + bundle.getString("numero_semana"));
            setTitle(bundle.getString("nombre_proyecto"));
            id_semana = bundle.getString("id_semana");
            if(oMPerfilSemana.getTitulo() == ""){
                new LoadDataSemana().execute();
            }
            else{
                pager_semana(oMPerfilSemana);
            }
        }
        else if(bundle.getString("tipo").equals("proyecto")){

            setTitle("Proyect Detail");

            numberWeek.setVisibility(View.GONE);
            txtNombre = (TextView) findViewById(R.id.txtNombre);
            totalSemanas = (TextView) findViewById(R.id.totalSemanas);
            fechaInicio = (TextView) findViewById(R.id.fechaInicio);

            id_proyecto = bundle.getString("id_proyecto");
            if(oMPerfilProyecto.getNombre_proyecto() == ""){
                new LoadDataProyecto().execute();
            }
            else{
                pager_proyecto(oMPerfilProyecto);
            }
        }
    }

    class LoadDataSemana extends AsyncTask<String,String,MPerfilSemana> {
        @Override
        protected MPerfilSemana doInBackground(String... params) {
            oConsult = new consult();
            oMPerfilSemana = oConsult.perfilSemana(id_semana);
            return oMPerfilSemana;
        }

        @Override
        protected void onPostExecute(MPerfilSemana array){
            super.onPostExecute(array);
            pager_semana(array);
        }
    }

    class LoadDataProyecto extends AsyncTask<String,String,MPerfilProyecto> {
        @Override
        protected MPerfilProyecto doInBackground(String... params) {
            oConsult = new consult();
            oMPerfilProyecto = oConsult.perfilProyectoPrimeraSemana(id_proyecto);
            return oMPerfilProyecto;
        }

        @Override
        protected void onPostExecute(MPerfilProyecto array){
            super.onPostExecute(oMPerfilProyecto);
//            pager_proyecto(array);
            pager.setPageTransformer(true, new DepthPageTransformer());
            MyFragmentPagerAdapter adapter = new MyFragmentPagerAdapter(getSupportFragmentManager(),detailProject.this);

            txtNombre.setText(oMPerfilProyecto.getNombre_proyecto());
            avance.setText(oMPerfilProyecto.getDescripcion());
            totalSemanas.setText(String.valueOf(oMPerfilProyecto.getTotal_semanas()) + ((oMPerfilProyecto.getTotal_semanas()>1)?" WEEKS":" WEEK"));
            fechaInicio.setText(oMPerfilProyecto.getFecha_inicio());
            titulo.setText(oMPerfilProyecto.getSub_titulo());

            for (int i = 0; i < oMPerfilProyecto.getFoto().size(); i++) {
                adapter.addFragment(ViewPagerFragmentProyect.newInstance(oMPerfilProyecto.getFoto().get(i)));
                pager.setAdapter(adapter);
            }
        }
    }

    public void pager_semana(MPerfilSemana array){
        pager.setPageTransformer(true, new DepthPageTransformer());
        MyFragmentPagerAdapter adapter = new MyFragmentPagerAdapter(getSupportFragmentManager(),detailProject.this);

        titulo.setText(array.getTitulo());
        avance.setText(array.getAvance());

        for (int i = 0; i < array.getFoto().size(); i++) {
            adapter.addFragment(ViewPagerFragmentProyect.newInstance(array.getFoto().get(i)));
            pager.setAdapter(adapter);
        }
    }

    public void pager_proyecto(MPerfilProyecto array){

        pager.setPageTransformer(true, new DepthPageTransformer());
        MyFragmentPagerAdapter adapter = new MyFragmentPagerAdapter(getSupportFragmentManager(),detailProject.this);

        txtNombre.setText(array.getNombre_proyecto());
        avance.setText(array.getDescripcion());
        totalSemanas.setText(String.valueOf(array.getTotal_semanas()) + ((array.getTotal_semanas()>1)?" WEEKS":" WEEK"));
        fechaInicio.setText(array.getFecha_inicio());
        titulo.setText(array.getSub_titulo());

//        generate_pager(array.getFoto());
//        pager.setPageTransformer(true, new DepthPageTransformer());
//        MyFragmentPagerAdapter adapter = new MyFragmentPagerAdapter(getSupportFragmentManager(),detailProject.this);
        for (int i = 0; i < array.getFoto().size(); i++) {
            adapter.addFragment(ViewPagerFragmentProyect.newInstance(array.getFoto().get(i)));
            pager.setAdapter(adapter);
        }
    }

    public void generate_pager(ArrayList<String> foto){

        pager.setPageTransformer(true, new DepthPageTransformer());
        MyFragmentPagerAdapter adapter = new MyFragmentPagerAdapter(getSupportFragmentManager(),detailProject.this);
        /*for (int i = 0; i < foto.size(); i++) {
            adapter.addFragment(ViewPagerFragmentProyect.newInstance(foto.get(i)));
            pager.setAdapter(adapter);
        }*/

        for (int i = 0; i < foto.size(); i++) {
            adapter.addFragment(ViewPagerFragmentProyect.newInstance(foto.get(i)));
            pager.setAdapter(adapter);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_detail_proyecto, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                oMPerfilSemana = new MPerfilSemana("","","");
                oMPerfilProyecto = new MPerfilProyecto("","","",0,"");
                return true;
            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void goBack(){
        finish();
        overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        oMPerfilSemana = new MPerfilSemana("","","");
        oMPerfilProyecto = new MPerfilProyecto("","","",0,"");
    }
}
