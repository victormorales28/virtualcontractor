package us.virtualcontractor.virtualcontractor;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import us.virtualcontractor.virtualcontractor.db.consult;
import us.virtualcontractor.virtualcontractor.model.MUsuario;
import us.virtualcontractor.virtualcontractor.util.AppPreferences;

/**
 * Created by ucweb02 on 24/04/2015.
 */
public class CustomModalLogin extends Dialog implements View.OnClickListener, View.OnFocusChangeListener{
    private ImageView imgLogo;
    private Context myContext;
    private EditText txtUserName;
    private EditText txtPassword;
    private EditText txtRecoverPassword;
    private TextView txtForgotPW;
    private TextView txtBackToLogin;
    private CheckBox chkRememberMe;
    private Button btnLogin;
    private Button btnRecoverPassword;

    private ViewFlipper viewFlipper;
    private AppPreferences shrPref;

    private final int[] idInputTextLogin = new int[]{R.id.txtUserName, R.id.txtLoginPassword};
    private final String[] msgErrorInputTextLogin = new String[]{"UserName or Email is required!", "Password is required!"};

    private final int[] idInputTextRecover = new int[]{R.id.txtRecoverPassword};
    private final String[] msgErrorInputTextRecover = new String[]{"UserName or Email is required!"};

    private consult oConsult;
    private ProgressDialog dlg;

    public CustomModalLogin(Context context, int theme) {
        super(context, theme);
        this.myContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setContentView(R.layout.modal_login);

        imgLogo = (ImageView)findViewById(R.id.imgLogoLogin);
        txtUserName = (EditText)findViewById(R.id.txtUserName);
        txtPassword = (EditText)findViewById(R.id.txtLoginPassword);
        txtRecoverPassword = (EditText)findViewById(R.id.txtRecoverPassword);
        txtForgotPW = (TextView)findViewById(R.id.txtForgotPW);
        txtBackToLogin = (TextView)findViewById(R.id.txtBackToLogin);
        chkRememberMe = (CheckBox)findViewById(R.id.chkRememberMe);
        btnLogin = (Button)findViewById(R.id.btnLogin);
        btnRecoverPassword = (Button)findViewById(R.id.btnRecoverPassword);

        viewFlipper = (ViewFlipper)findViewById(R.id.vfLogin);

        txtUserName.setOnFocusChangeListener(this);
        txtPassword.setOnFocusChangeListener(this);
        btnLogin.setOnClickListener(this);
        txtForgotPW.setOnClickListener(this);
        txtBackToLogin.setOnClickListener(this);
        btnRecoverPassword.setOnClickListener(this);

        shrPref = new AppPreferences(myContext, "LoginData");

        dlg = new ProgressDialog(myContext);
        dlg.setCancelable(false);
        dlg.setMessage("Loading...");
        dlg.setTitle("Login");
    }


    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnLogin:
                if (!validateIsEmptyInputs(-1, idInputTextLogin, msgErrorInputTextLogin)) {
                    dlg.show();
                    new LoadData().execute(txtUserName.getText().toString(), txtPassword.getText().toString());
                }
                break;
            case R.id.txtForgotPW:
                viewFlipper.setInAnimation(myContext, R.anim.scale_min_to_max);
                viewFlipper.setOutAnimation(myContext, R.anim.scale_max_to_min);
                viewFlipper.showNext();
                break;
            case R.id.txtBackToLogin:
                viewFlipper.setInAnimation(myContext, R.anim.scale_min_to_max);
                viewFlipper.setOutAnimation(myContext, R.anim.scale_max_to_min);
                viewFlipper.showNext();
                break;
            case R.id.btnRecoverPassword:
                validateIsEmptyInputs(idInputTextRecover[0], idInputTextRecover, msgErrorInputTextRecover);
                break;
        }
    }

    public class LoadData extends AsyncTask<String, String, MUsuario>{
        @Override
        protected MUsuario doInBackground(String... params) {
            oConsult = new consult();
            MUsuario objUsuario = oConsult.login(params[0], params[1]);
            return objUsuario;
        }

        @Override
        protected void onPostExecute(MUsuario obj) {
            super.onPostExecute(obj);
            validarLogin(obj);
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }
    }

    private void validarLogin(MUsuario obj){
        if (obj != null) {
            if (chkRememberMe.isChecked()) {
                shrPref.setPreferences("IdUser", Integer.toString(obj.getId_usuario()));
                shrPref.setPreferences("UserName", txtUserName.getText().toString().trim());
                shrPref.setPreferences("PassWord", txtPassword.getText().toString().trim());
                shrPref.setPreferences("TypeUser", String.valueOf(obj.getTipousuario()));
            } else {
                if (shrPref.getPreferences("UserName", "") != "") {
                    shrPref.setPreferences("IdUser", "");
                    shrPref.setPreferences("UserName", "");
                    shrPref.setPreferences("PassWord", "");
                    shrPref.setPreferences("TypeUser", "");
                }
            }
            Intent app = new Intent(myContext, MainApp.class);
            app.putExtra("TypeUser", String.valueOf(obj.getTipousuario()));
            myContext.startActivity(app);
            dlg.dismiss();
            dismiss();
            ((Activity) myContext).finish();
        } else {
            dlg.dismiss();
            Toast.makeText(myContext, "Username or password is incorrect.", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (v.getId() == R.id.txtUserName && !hasFocus) {
            validateIsEmptyInputs(idInputTextLogin[0], idInputTextLogin, msgErrorInputTextLogin);
        }
        if (v.getId() == R.id.txtLoginPassword && !hasFocus) {
            validateIsEmptyInputs(idInputTextLogin[1], idInputTextLogin, msgErrorInputTextLogin);
        }
    }

    private Boolean validateIsEmptyInputs(int validateInput, int[] idInputText, String[] msgErrorInputText) {

        Boolean validate = false;
        EditText txt;

        for (int i = 0; i < idInputText.length; i++) {
            if (validateInput != -1 && validateInput != idInputText[i]) continue;
            txt = (EditText) findViewById(idInputText[i]);
            if (txt.getText().toString().trim().length() == 0) {
                txt.setError(msgErrorInputText[i]);
                validate = true;
            }
        }

        return validate;
    }
}
