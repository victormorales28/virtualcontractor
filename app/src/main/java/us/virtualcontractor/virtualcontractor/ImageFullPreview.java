package us.virtualcontractor.virtualcontractor;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

/**
 * Created by ucweb02 on 19/05/2015.
 */
public class ImageFullPreview extends Dialog {

    private String imgPath;
    private ImageView imgView;
    private Button btnOk;

    public ImageFullPreview(Context context, int theme, String path) {
        super(context, theme);
        this.imgPath = path;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setContentView(R.layout.modal_full_image);

        imgView = (ImageView) findViewById(R.id.imgFullPreview);
        btnOk = (Button) findViewById(R.id.Button01);
        BitmapFactory.Options options = null;
        options = new BitmapFactory.Options();
        options.inSampleSize = 3;
        Bitmap bitmap = BitmapFactory.decodeFile(imgPath,options);
        imgView.setImageBitmap(bitmap);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

}
