package us.virtualcontractor.virtualcontractor.viewPager;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import us.virtualcontractor.virtualcontractor.R;

/**
 * Created by ucweb01 on 23/04/2015.
 */
public class ViewPagerFragmentCamera extends Fragment {

    public static ViewPagerFragmentCamera newInstance() {
        // Instantiate a new fragment
        ViewPagerFragmentCamera fragment = new ViewPagerFragmentCamera();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.galery_camera_img, container, false);
        return rootView;
    }
}
