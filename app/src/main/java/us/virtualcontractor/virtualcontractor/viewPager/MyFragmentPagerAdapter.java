package us.virtualcontractor.virtualcontractor.viewPager;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ucweb01 on 21/04/2015.
 */
public class MyFragmentPagerAdapter extends FragmentPagerAdapter {

    List<Fragment> fragments;
    Context context;

    public MyFragmentPagerAdapter(FragmentManager manager,Context context) {
        super(manager);
        this.context=context;
        this.fragments = new ArrayList<Fragment>();
    }

    public void addFragment(Fragment fragment) {
        this.fragments.add(fragment);//Agregamos fragment a nuestra list
    }

    @Override
    public Fragment getItem(int arg0) {
        return this.fragments.get(arg0);//Obtenemos un item de nuestra list
    }

    @Override
    public int getCount() {
        return this.fragments.size();
    }
}
