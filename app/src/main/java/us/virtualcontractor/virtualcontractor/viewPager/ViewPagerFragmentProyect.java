package us.virtualcontractor.virtualcontractor.viewPager;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;

import us.virtualcontractor.virtualcontractor.R;

/**
 * Created by ucweb01 on 21/04/2015.
 */
public class ViewPagerFragmentProyect extends Fragment {

    private final String foto;

    public static ViewPagerFragmentProyect newInstance(String foto) {
        // Instantiate a new fragment
        ViewPagerFragmentProyect fragment = new ViewPagerFragmentProyect(foto);
        return fragment;
    }

    public ViewPagerFragmentProyect(String foto) {
        this.foto = foto;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.galery_project, container, false);
        ImageView imgViewfoto = (ImageView) rootView.findViewById(R.id.foto);
        UrlImageViewHelper.setUrlDrawable(imgViewfoto, "http://virtualc.uc-web.mobi/web_service/fotos_semana/" + foto);

        return rootView;
    }
}









