package us.virtualcontractor.virtualcontractor;

import java.util.ArrayList;
import java.util.Arrays;

import us.virtualcontractor.virtualcontractor.db.consult;
import us.virtualcontractor.virtualcontractor.model.MLocation;

/**
 * Created by ucweb02 on 17/04/2015.
 */
public class ListAlertDialog {

    public ArrayList<String> listaRegistrarComo = new ArrayList<String>(Arrays.asList("Constructor", "Client"));

    public ArrayList<String> listaProfesiones = new ArrayList<String>(Arrays.asList("Profesion 1", "Profesion 2", "Profesion 3", "Profesion 4", "Profesion 5"));

    public ArrayList<String> listaSemanasProyectos = new ArrayList<String>(Arrays.asList("One Week", "Two Week", "Three Week",
            "Four Week", "Five Week", "Six Week", "Seven Week", "Eight Week", "Nine Week", "Ten Week"));

}
