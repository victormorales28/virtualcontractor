package us.virtualcontractor.virtualcontractor;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TabHost;

public class HomeApp extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_entry_app2);
        getSupportActionBar().hide();
//        getSupportActionBar().setHomeButtonEnabled(true);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getActionBar().setIcon(R.mipmap.ic_launcher);
        getSupportActionBar().setLogo(getResources().getDrawable(R.mipmap.ic_launcher));
        getSupportActionBar().setTitle(getResources().getString(R.string.title_EntryApp));

        //TABS
        Resources res = getResources();

        final TabHost tabs=(TabHost)findViewById(R.id.tabsMenuEntrar);
        tabs.setup();

        TabHost.TabSpec spec=tabs.newTabSpec("mitab1");
        spec.setContent(R.id.tab1);
        //spec.setIndicator("",res.getDrawable(android.R.drawable.ic_btn_sspeak_now));
        spec.setIndicator("",res.getDrawable(R.drawable.selector_tabhost_contratista));
        tabs.addTab(spec);

        spec=tabs.newTabSpec("mitab2");
        spec.setContent(R.id.tab2);
        spec.setIndicator("",res.getDrawable(R.drawable.selector_tabhost_cliente));
        tabs.addTab(spec);

        spec=tabs.newTabSpec("mitab3");
        spec.setContent(R.id.tab3);
        spec.setIndicator("",res.getDrawable(R.drawable.selector_tabhost_registrarse));
        tabs.addTab(spec);

        spec=tabs.newTabSpec("mitab4");
        spec.setContent(R.id.tab4);
        spec.setIndicator("",res.getDrawable(R.drawable.selector_tabhost_salir));
        tabs.addTab(spec);

//        tabs.setCurrentTab(0);

        tabs.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String arg0) {
//                CustomModalLogin modal = new CustomModalLogin(HomeApp.this, R.style.PauseDialog);
                CustomModalLogin modal = new CustomModalLogin(HomeApp.this, 0);
                switch (tabs.getCurrentTab()){
                    case 0:
                        modal.show();
                        break;
                    case 1:
                        modal.show();
                        break;
                    case 2:
                        Intent registroUsuario = new Intent(getApplicationContext(), RegistrarUsuario.class);
//                        registroUsuario.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(registroUsuario);
//                        overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
                        HomeApp.this.overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);

//                        AnimatorSet set = (AnimatorSet) AnimatorInflater.loadAnimator(getApplicationContext(),R.animator.property_animator);
//                        set.setTarget(this);
//                        set.start();
                        break;
                    case 3:
                        finish();
                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_entry_app, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
