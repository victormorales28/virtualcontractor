package us.virtualcontractor.virtualcontractor;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import us.virtualcontractor.virtualcontractor.model.MUsuario;
import us.virtualcontractor.virtualcontractor.secciones.sec_contractor_blog;
import us.virtualcontractor.virtualcontractor.secciones.sec_create_a_project;
import us.virtualcontractor.virtualcontractor.secciones.sec_find_contractors;
import us.virtualcontractor.virtualcontractor.secciones.sec_home;
import us.virtualcontractor.virtualcontractor.secciones.sec_my_contractors;
import us.virtualcontractor.virtualcontractor.secciones.sec_my_profile;
import us.virtualcontractor.virtualcontractor.secciones.sec_my_proyects;
import us.virtualcontractor.virtualcontractor.secciones.sec_request_quote;
import us.virtualcontractor.virtualcontractor.util.NavDrawerItem;
import us.virtualcontractor.virtualcontractor.util.NavDrawerListAdapter;


public class MainApp extends ActionBarActivity {

    public DrawerLayout mDrawerLayout;
    public ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    // nav drawer title
    private CharSequence mDrawerTitle;

    // used to store app title
    private CharSequence mTitle;

    // slide menu items
    private String[] navMenuTitles;
    private TypedArray navMenuIcons;

    private ArrayList<NavDrawerItem> navDrawerItems;
    private NavDrawerListAdapter adapter;

    private View vwUi;
    //private LinearLayout inicio;

    private MUsuario objUsuario;

    private static int frag = 1;

    private OnBaseActivityResult baseActivityResult;

    public static final int BASE_RESULT_RCODE = 111;

    private String tipo_usuario;

    public OnBaseActivityResult getBaseActivityResult() {
        return baseActivityResult;
    }

    public void setBaseActivityResult(OnBaseActivityResult baseActivityResult) {
        this.baseActivityResult = baseActivityResult;
    }

    public interface OnBaseActivityResult{
        void onBaseActivityResult(int requestCode, int resultCode, Intent data);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        vwUi =  this.getWindow().getDecorView();
        showSystemUI();
        setContentView(R.layout.activity_main);

        mTitle = mDrawerTitle = getTitle();

        Intent i = getIntent();
        Bundle b = i.getExtras();

        if (b != null) {
            tipo_usuario = b.getString("TypeUser").trim();
            if (tipo_usuario.equals("1")) {
                navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items_contratista);
            } else if (tipo_usuario.equals("2")) {
                navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items_cliente);
            }
        }

        // nav drawer icons from resources
        navMenuIcons = getResources().obtainTypedArray(R.array.nav_drawer_icons);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.list_slidermenu);

        navDrawerItems = new ArrayList<NavDrawerItem>();

        // agregar un nuevo item al menu deslizante
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons.getResourceId(0, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons.getResourceId(1, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons.getResourceId(2, -1)));//navMenuIcons.getResourceId(2, -1), true, "Estrenos"
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons.getResourceId(3, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons.getResourceId(4, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], navMenuIcons.getResourceId(5, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[6], navMenuIcons.getResourceId(6, -1)));

        // Recycle the typed array
        navMenuIcons.recycle();

        mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

        // setting the nav drawer list adapter
        adapter = new NavDrawerListAdapter(getApplicationContext(),
                navDrawerItems);
        mDrawerList.setAdapter(adapter);

        // enabling action bar app icon and behaving it as toggle button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(R.string.app_name);

        //Cambiar Icon
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.drawable.logo, //nav menu toggle icon
                R.string.app_name, // nav drawer open - description for accessibility
                R.string.app_name // nav drawer close - description for accessibility
        ) {
            public void onDrawerClosed(View view) {
                getSupportActionBar().setTitle(mTitle);//mTitle
                // calling onPrepareOptionsMenu() to show action bar icons
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                getSupportActionBar().setTitle(mDrawerTitle);//mDrawerTitle
                // calling onPrepareOptionsMenu() to hide action bar icons
                invalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        if (savedInstanceState == null) {
            // on first time display view for first nav item
            displayView(7);
            int orientation = getResources().getConfiguration().orientation;
        }
    }

    private void showSystemUI() {
        vwUi.setSystemUiVisibility(View.SYSTEM_UI_FLAG_IMMERSIVE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        Toast.makeText(this, "Result Main App", Toast.LENGTH_LONG).show();
        if (getBaseActivityResult() != null) {
            getBaseActivityResult().onBaseActivityResult(requestCode, resultCode, data);
//            setBaseActivityResult(null);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle action bar actions click
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private class SlideMenuClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            // display view for selected nav drawer item
            displayView(position);
        }
    }

    /* *
     * Called when invalidateOptionsMenu() is triggered
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // if nav drawer is opened, hide the action items
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

    /**
     * Diplaying fragment view for selected nav drawer list item
     * */
    public void displayView(int position){
        // update the main content by replacing fragments
        //getSupportActionBar().setTitle(mDrawerTitle);
//        int fragmentActive = 0;
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new sec_my_profile(MainApp.this);
                break;
            case 1:
                fragment = new sec_my_proyects();
                break;
            case 2:
                if (frag == 1) {
                    fragment = new sec_create_a_project(getSupportFragmentManager(), MainApp.this);
                    frag = 2;
                }
                break;
            case 3:
                fragment = (tipo_usuario=="1")? new sec_contractor_blog():new sec_find_contractors();
                break;
            case 4:
                fragment = new sec_my_contractors();
                break;
            case 5:
                fragment = new sec_request_quote(MainApp.this);
                break;
            case 6:
                SharedPreferences shrPref = getApplicationContext().getSharedPreferences("LoginData", Context.MODE_PRIVATE);
                SharedPreferences.Editor editorShrPref = shrPref.edit();
                editorShrPref.putString("UserName", "");
                editorShrPref.putString("PassWord", "");
                editorShrPref.commit();
                Intent app = new Intent(this, HomeApp.class);
                startActivity(app);
                finish();
                break;
            case 7:
                fragment = new sec_home(this);
                break;
            default:
                break;
        }
        if (fragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).commit();
        }

        //inicio.setVisibility(View.INVISIBLE);
        // update selected item and title, then close the drawer
        if (position!=6){
            //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
            mDrawerList.setItemChecked(position, true);
        }

        mDrawerList.setSelection(position);
        setTitle(navMenuTitles[position]);
        mDrawerLayout.closeDrawer(mDrawerList);

    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getSupportActionBar().setTitle(mTitle);//mTitle
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
//        moveTaskToBack(false);
        displayView(7);
    }
}
