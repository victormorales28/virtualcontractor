package us.virtualcontractor.virtualcontractor.secciones;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import us.virtualcontractor.virtualcontractor.MainApp;
import us.virtualcontractor.virtualcontractor.R;
import us.virtualcontractor.virtualcontractor.db.ConnectServer;
import us.virtualcontractor.virtualcontractor.db.consult;
import us.virtualcontractor.virtualcontractor.dialog.CustomAlertDialog;
import us.virtualcontractor.virtualcontractor.interfaces.EventCustomAlertDialog;
import us.virtualcontractor.virtualcontractor.interfaces.ServerConnectionListener;
import us.virtualcontractor.virtualcontractor.interfaces.TaskImageListener;
import us.virtualcontractor.virtualcontractor.model.MLocation;
import us.virtualcontractor.virtualcontractor.model.MPerfilUsuario;
import us.virtualcontractor.virtualcontractor.model.MProfesiones;
import us.virtualcontractor.virtualcontractor.util.AppPreferences;
import us.virtualcontractor.virtualcontractor.util.AsyncTaskImage;
import us.virtualcontractor.virtualcontractor.util.EmailValidator;
import us.virtualcontractor.virtualcontractor.util.IDViews;
import us.virtualcontractor.virtualcontractor.util.ListAlertDialog;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by ucweb01 on 10/04/2015.
 */
public class sec_my_profile extends Fragment implements View.OnClickListener, EventCustomAlertDialog, TaskImageListener, MainApp.OnBaseActivityResult, ServerConnectionListener {

    private EditText txtRegistrarComo;
    private EditText txtCompany;
    private EditText txtState;
    private EditText txtCity;
    private EditText txtProfession;
    private EditText txtEmail;
    private EditText txtPassword;
    private EditText txtConfirmPassword;
    private Button btnSaveProfile;
    private Button btnUploadAvatar;
    private AlertDialog myAlertDialog;
    private AlertDialog.Builder myAlertBuilder;
    private static int alertSelectedItem = 0;
    private static int alertSelectedItemStates = 0;
    private Boolean lEstado = false;
    private EditText txtEditViewDialog;
    private ListAlertDialog objListAlertDialog = new ListAlertDialog();
    private ArrayList<String> listAlertDialog;
    private EmailValidator emailValidator;

    private ArrayList<String> listaEstados = null;
    private ArrayList<String> listaCiudades = null;
    private consult oConsult;
    private MLocation patternLocation;

    private ArrayList<String> listaProfesiones = null;
    private MProfesiones objProfesiones = null;

    private ArrayList<String> encodeImageString = new ArrayList<String>();
    private String pathImage = "";
    private AsyncTaskImage taskImage;
    private ConnectServer cnServer;

    private MPerfilUsuario mPerfilUsuario;

    private static int RESULT_LOAD_CAMERA = 1;
    private static int RESULT_LOAD_GALLERY = 2;

    private Uri uriAvatar;

    private ProgressDialog progressDialog;

    private View myView;
    private Context myContext;

    private CustomAlertDialog ctmAlertDialog;
    private int indexShowDialog = 0;

    private String[] idsItemsSelected = new String[4];
    private boolean[] indexItemsChecked = null;

    public sec_my_profile(Context ctx) {
        this.myContext = ctx;
    }

    public Context getMyContext() {
        return myContext;
    }

    private AppPreferences shrPreferences;

    private ProgressDialog loadProfileDialog;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        new LoadData().execute();

        View rootView = inflater.inflate(R.layout.sec_my_profile, container, false);
        myView = rootView;
        txtRegistrarComo = (EditText) rootView.findViewById(R.id.txtRegisterAs);
        txtCompany = (EditText) rootView.findViewById(R.id.txtCompany);
        txtState = (EditText) rootView.findViewById(R.id.txtState);
        txtCity = (EditText) rootView.findViewById(R.id.txtCity);
        txtProfession = (EditText) rootView.findViewById(R.id.txtProfession);
        txtEmail = (EditText) rootView.findViewById(R.id.txtEmail);
        btnSaveProfile = (Button) rootView.findViewById(R.id.btnSaveProfile);
        btnUploadAvatar = (Button) rootView.findViewById(R.id.btnUploadAvatar);

        myAlertBuilder = new AlertDialog.Builder(this.myContext);
        shrPreferences = new AppPreferences(getMyContext(), "LoginData");

        btnSaveProfile.setOnClickListener(this);
        txtRegistrarComo.setOnClickListener(this);
        txtState.setOnClickListener(this);
        txtCity.setOnClickListener(this);
        txtProfession.setOnClickListener(this);
        btnUploadAvatar.setOnClickListener(this);

        emailValidator = new EmailValidator();
        setHasOptionsMenu(true);

        ((MainApp) getMyContext()).setBaseActivityResult(sec_my_profile.this);

        loadProfileDialog = new ProgressDialog(myContext);
        loadProfileDialog.setMessage("Loading profile...");
        loadProfileDialog.setCancelable(false);
        loadProfileDialog.show();

        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.txtRegisterAs:
                ctmAlertDialog = new CustomAlertDialog(myContext, objListAlertDialog.listaRegistrarComo, "Select type user.", "single");
                ctmAlertDialog.handlerEvents(sec_my_profile.this);
                ctmAlertDialog.show();
                indexShowDialog = 1;
                break;
            case R.id.txtState:
                if (listaEstados != null) {
                    ctmAlertDialog = new CustomAlertDialog(myContext, listaEstados, "Select State.", "single");
                    ctmAlertDialog.handlerEvents(sec_my_profile.this);
                    ctmAlertDialog.show();
                    indexShowDialog = 2;
                    txtCity.setText("");
                }
                break;
            case R.id.txtCity:
                if (String.valueOf(txtState.getText()).isEmpty()) {
                    Toast.makeText(myContext, "Must select a state.", Toast.LENGTH_SHORT).show();
                } else {
                    ctmAlertDialog = new CustomAlertDialog(myContext, listaCiudades, "Select City.", "single");
                    ctmAlertDialog.handlerEvents(sec_my_profile.this);
                    ctmAlertDialog.show();
                    indexShowDialog = 3;
                }
                break;
            case R.id.txtProfession:
                txtProfession.setText("");
                if (listaProfesiones != null) {
                    ctmAlertDialog = new CustomAlertDialog(myContext, listaProfesiones, "Select profession.", "multiple");
                    ctmAlertDialog.handlerEvents(sec_my_profile.this);
                    ctmAlertDialog.setMaxItemsSelected(5);
                    ctmAlertDialog.setIndexItemsChecked(indexItemsChecked);
                    ctmAlertDialog.show();
                    indexShowDialog = 4;
                }
                break;
            case R.id.btnSaveProfile:
                validateFields();
                break;
            case R.id.btnUploadAvatar:
                ArrayList<String> lista = new ArrayList<String>(Arrays.asList("Camera", "Gallery"));
                ctmAlertDialog = new CustomAlertDialog(myContext, lista, "Select source.", "single");
                ctmAlertDialog.handlerEvents(sec_my_profile.this);
                ctmAlertDialog.show();
                indexShowDialog = 5;
                break;
        }
    }

    @Override
    public void onAsyncTaskImageFinished(ArrayList<String> encodeImages) {
        if (encodeImages.size() > 0) {
            uploadToServer(encodeImages.get(0));
        } else {
            Toast.makeText(getMyContext(), "Error encode images, null data.", Toast.LENGTH_SHORT).show();
        }
    }

    private void uploadToServer(String avatar){
        cnServer = new ConnectServer(getMyContext(), "updateUser.php");
        cnServer.handleEvents(sec_my_profile.this);
        cnServer.setProgressDialog(progressDialog);
        if (idsItemsSelected[0] == "1") {
            loadParamsContractorHttpPost();
        } else {
            loadParamsClientHttpPost();
        }
        cnServer.addParameter("avatar", avatar);
        cnServer.Upload();
    }

    @Override
    public void serverConnectionOnSuccess(String response) {
        try {
            JSONObject json = new JSONObject(response);
            JSONArray data = json.optJSONArray("update");
            JSONObject obj = data.getJSONObject(0);
            AlertDialog dlgRegister;
            AlertDialog.Builder dlgBRegister = new AlertDialog.Builder(getMyContext());
            dlgBRegister.setTitle("Update Profile");
            dlgBRegister.setMessage(obj.optString("message").toString());
            dlgBRegister.setCancelable(false);
            dlgBRegister.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                }
            });
            dlgRegister = dlgBRegister.create();
            dlgRegister.show();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void loadParamsClientHttpPost(){
        cnServer.addParameter("id_usuario", shrPreferences.getPreferences("IdUser", "").toString());
        cnServer.addParameter("tipo_usuario", idsItemsSelected[0]);
        cnServer.addParameter("nombres", getTextByID(IDViews.ID_VALIDATE_CLIENT[0]));
        cnServer.addParameter("apellidos", getTextByID(IDViews.ID_VALIDATE_CLIENT[1]));
        cnServer.addParameter("telefono", getTextByID(IDViews.ID_VALIDATE_CLIENT[2]));
        cnServer.addParameter("movil", getTextByID(IDViews.ID_VALIDATE_CLIENT[3]));
        cnServer.addParameter("id_ciudad", idsItemsSelected[2]);
        cnServer.addParameter("email", getTextByID(IDViews.ID_VALIDATE_CLIENT[6]));
        cnServer.addParameter("contrasenia", getTextByID(IDViews.ID_VALIDATE_CLIENT[7]));
    }

    private void loadParamsContractorHttpPost(){
        cnServer.addParameter("id_usuario", shrPreferences.getPreferences("IdUser", "").toString());
        cnServer.addParameter("tipo_usuario", idsItemsSelected[0]);
        cnServer.addParameter("nombres", getTextByID(IDViews.ID_VALIDATE_CONTRACTOR[0]));
        cnServer.addParameter("apellidos", getTextByID(IDViews.ID_VALIDATE_CONTRACTOR[1]));
        cnServer.addParameter("compania", getTextByID(IDViews.ID_VALIDATE_CONTRACTOR[2]));
        cnServer.addParameter("telefono", getTextByID(IDViews.ID_VALIDATE_CONTRACTOR[3]));
        cnServer.addParameter("movil", getTextByID(IDViews.ID_VALIDATE_CONTRACTOR[4]));
        cnServer.addParameter("id_ciudad", idsItemsSelected[2]);
        cnServer.addParameter("id_profesion", idsItemsSelected[3]);
        cnServer.addParameter("email", getTextByID(IDViews.ID_VALIDATE_CONTRACTOR[8]));
        cnServer.addParameter("contrasenia", getTextByID(IDViews.ID_VALIDATE_CONTRACTOR[9]));
    }

    @Override
    public void onBaseActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            // When an Image is picked
            if (requestCode == RESULT_LOAD_GALLERY && resultCode == Activity.RESULT_OK && null != data) { // GALERIA
                // Get the Image from data

                uriAvatar = data.getData();
                String[] filePathColumn = { MediaStore.Images.Media.DATA };

                // Get the cursor
                Cursor cursor = getMyContext().getContentResolver().query(uriAvatar, filePathColumn, null, null, null);
                // Move to first row
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                pathImage = cursor.getString(columnIndex);
                cursor.close();
            }

            if (requestCode == RESULT_LOAD_GALLERY || requestCode == RESULT_LOAD_CAMERA) {
                if (resultCode == Activity.RESULT_OK) {
                    Toast.makeText(getMyContext(), "Correctly selected image.", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getMyContext(), "Image selection canceled.", Toast.LENGTH_LONG).show();
                }
            }
//          showDialogImagePreview();
        } catch (Exception e) {
            Toast.makeText(getMyContext(), "Something went wrong", Toast.LENGTH_LONG).show();
        }
    }

    class LoadData extends AsyncTask<String,String,String> {
        @Override
        protected String doInBackground(String... params) {
            oConsult = new consult();
            patternLocation = oConsult.getEstadoCiudad();
            listaEstados = patternLocation.getListaEstados();
            objProfesiones = oConsult.getProfesiones();
            listaProfesiones = objProfesiones.getListaProfesiones();
            mPerfilUsuario = oConsult.getPerfilUsuario(shrPreferences.getPreferences("IdUser", ""), shrPreferences.getPreferences("TypeUser", ""));
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            loadProfileUser();
        }
    }

    private void loadProfileUser(){
        if (mPerfilUsuario != null) {
            String tipoUsuario = "";
            if (shrPreferences.getPreferences("TypeUser", "").equals("1")) {
                tipoUsuario = "Contractor";
                idsItemsSelected[0] = "1";
            } else if (shrPreferences.getPreferences("TypeUser", "").equals("2")) {
                tipoUsuario = "Client";
                idsItemsSelected[0] = "2";
            }
            setTextByID(R.id.txtRegisterAs, tipoUsuario);
            setTextByID(IDViews.ID_VALIDATE_CONTRACTOR[0], mPerfilUsuario.getNombre());
            setTextByID(IDViews.ID_VALIDATE_CONTRACTOR[1], mPerfilUsuario.getApellido());
            setTextByID(IDViews.ID_VALIDATE_CONTRACTOR[2], mPerfilUsuario.getCompania());
            setTextByID(IDViews.ID_VALIDATE_CONTRACTOR[3], mPerfilUsuario.getTelefono());
            setTextByID(IDViews.ID_VALIDATE_CONTRACTOR[4], mPerfilUsuario.getMovil());
            setTextByID(IDViews.ID_VALIDATE_CONTRACTOR[5], patternLocation.getTextStateByID(mPerfilUsuario.getId_estado()));
            setTextByID(IDViews.ID_VALIDATE_CONTRACTOR[6], patternLocation.getTextCityByID(mPerfilUsuario.getId_ciudad()));
            setTextByID(IDViews.ID_VALIDATE_CONTRACTOR[7], objProfesiones.getTextProfesiones(mPerfilUsuario.getProfesion()));
            setTextByID(IDViews.ID_VALIDATE_CONTRACTOR[8], mPerfilUsuario.getEmail());
            setTextByID(IDViews.ID_VALIDATE_CONTRACTOR[9], shrPreferences.getPreferences("PassWord", ""));
            setTextByID(IDViews.ID_VALIDATE_CONTRACTOR[10], shrPreferences.getPreferences("PassWord", ""));
            idsItemsSelected[1] = mPerfilUsuario.getId_estado().toString();
            idsItemsSelected[2] = mPerfilUsuario.getId_ciudad().toString();
            idsItemsSelected[3] = objProfesiones.getIDSProfesion(mPerfilUsuario.getProfesion());
            if (idsItemsSelected[0] == "1") {
                txtCompany.setVisibility(View.VISIBLE);
                txtProfession.setVisibility(View.VISIBLE);
            } else {
                txtCompany.setVisibility(View.GONE);
                txtProfession.setVisibility(View.GONE);
            }
        } else {
            Toast.makeText(getMyContext(), "Profile Null", Toast.LENGTH_SHORT).show();
        }
        loadProfileDialog.dismiss();
    }

    public Boolean validateFields() {
        Boolean validarMensajeError = false;
        Boolean validarDatosUsuario = false;
        String mensaje = "", titulo = "Warning: Fields incorrect.";
        String contrasenia = "";
        String contraseniaConfirmar = "";
        if (idsItemsSelected[0] == "1") {
            validarDatosUsuario = validateIsEmptyInputs(IDViews.ID_VALIDATE_CONTRACTOR, -1, IDViews.MSG_ERROR_VALIDATE_CONTRACTOR);
            contrasenia = String.valueOf(getTextByID(IDViews.ID_VALIDATE_CONTRACTOR[9]));
            contraseniaConfirmar = String.valueOf(getTextByID(IDViews.ID_VALIDATE_CONTRACTOR[10]));
        } else if (idsItemsSelected[0] == "2") {
            validarDatosUsuario = validateIsEmptyInputs(IDViews.ID_VALIDATE_CLIENT, -1, IDViews.MSG_ERROR_VALIDATE_CLIENT);
            contrasenia = String.valueOf(getTextByID(IDViews.ID_VALIDATE_CLIENT[7]));
            contraseniaConfirmar = String.valueOf(getTextByID(IDViews.ID_VALIDATE_CLIENT[8]));
        }

        if (txtRegistrarComo.getText().toString().isEmpty()) {
            mensaje = "Type User is required.";
        } else {
            if (!validarDatosUsuario) {
                if (emailValidator.validate(String.valueOf(txtEmail.getText()))) {
                    if (contrasenia.equals(contraseniaConfirmar)) {
                        if (pathImage != "") {

                        }
                        validarMensajeError = true;
                        taskImage = new AsyncTaskImage(sec_my_profile.this);
                        ArrayList<String> imgPath = new ArrayList<String>(Arrays.asList(pathImage));
                        progressDialog = new ProgressDialog(myContext);
                        progressDialog.setCancelable(false);
                        progressDialog.show();
                        taskImage.setProgressDialog(progressDialog);
                        taskImage.execute(imgPath);
                    } else {
                        mensaje += "The passwords entered do not match.";
                    }
                } else {
                    if (contrasenia.equals(contraseniaConfirmar)) {
                        mensaje += "The entered email is not valid.";
                    } else {
                        mensaje += "The entered email is not valid.\n\n";
                        mensaje += "The passwords entered do not match.";
                    }
                }
            }
        }

        if (!validarMensajeError && !mensaje.isEmpty()) {
            AlertDialog.Builder ab = new AlertDialog.Builder(myContext);
            ab.setTitle(titulo)
                    .setMessage(mensaje)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            myAlertDialog.dismiss();
                        }
                    });
            myAlertDialog = ab.create();
            myAlertDialog.show();
        }

        return validarMensajeError;
    }

    private String getTextByID(int id){
        EditText txt = (EditText) myView.findViewById(id);
        return txt.getText().toString().trim();
    }

    private void setTextByID(int id, String value){
        EditText txt = (EditText) myView.findViewById(id);
        txt.setText(value);
    }

    private Boolean validateIsEmptyInputs(int[] idsValidate, int validateInput, String[] msgError) {
        Boolean validate = false;
        EditText txt;
        for (int i = 0; i < idsValidate.length; i++) {
            if (validateInput != -1 && validateInput != idsValidate[i]) continue;
            txt = (EditText) myView.findViewById(idsValidate[i]);
            if (txt.getText().toString().trim().length() == 0) {
                txt.setError(msgError[i]);
                validate = true;
            } else {
                txt.setError(null);
            }
        }
        return validate;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_perfil_usuario, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.EditProfile:
                Toast.makeText(myContext, "Edit Profile",Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onADSelectedItem(int index) {

    }

    @Override
    public void onADClickButtonOk() {
        switch (indexShowDialog){
            case 1:
                if (ctmAlertDialog.getIndexItemSelected() == 0) {
                    txtCompany.setVisibility(View.VISIBLE);
                    txtProfession.setVisibility(View.VISIBLE);
                    idsItemsSelected[0] = "1";
                } else {
                    txtCompany.setVisibility(View.GONE);
                    txtProfession.setVisibility(View.GONE);
                    idsItemsSelected[0] = "2";
                }
                txtRegistrarComo.setText(ctmAlertDialog.getStringItemSelected());
                break;
            case 2:
                txtState.setText(ctmAlertDialog.getStringItemSelected());
                idsItemsSelected[1] = String.valueOf(patternLocation.getIDState(ctmAlertDialog.getIndexItemSelected()));
                listaCiudades = patternLocation.getListaCiudadPorEstado(ctmAlertDialog.getIndexItemSelected());
                break;
            case 3:
                txtCity.setText(ctmAlertDialog.getStringItemSelected());
                idsItemsSelected[2] = String.valueOf(patternLocation.getIDCity(ctmAlertDialog.getIndexItemSelected()));
                break;
            case 4:
                txtProfession.setText(ctmAlertDialog.getStringItemsSelected());
                indexItemsChecked = ctmAlertDialog.getIndexItemsChecked();
                idsItemsSelected[3] = objProfesiones.getIDSProfesion(ctmAlertDialog.getIndexItemsSelected());
                break;
            case 5:
                if (ctmAlertDialog.getIndexItemSelected() == 0) {
                    File imagesFolder = new File(Environment.getExternalStorageDirectory(), "/Virtual Contractor/Media/Upload");
                    if (!imagesFolder.exists()) {
                        imagesFolder.mkdirs();
                    }
                    pathImage = Environment.getExternalStorageDirectory() + "/Virtual Contractor/Media/Upload/avatar.jpg";
                    File file = new File(pathImage);
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    uriAvatar = Uri.fromFile(file);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, uriAvatar);
                    getActivity().startActivityForResult(intent, RESULT_LOAD_CAMERA);
                } else {
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    getActivity().startActivityForResult(galleryIntent, RESULT_LOAD_GALLERY);
                }
                break;
        }
    }
}
