package us.virtualcontractor.virtualcontractor.secciones;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;
import android.widget.ViewFlipper;

import us.virtualcontractor.virtualcontractor.R;

/**
 * Created by ucweb01 on 15/04/2015.
 */
public class sec_request_quote extends Fragment {
    private ViewFlipper myVF;
    private Button btnSolicitarCot;
    private Context myContext;
    private View myView;

    public sec_request_quote(Context ctx) {
        this.myContext = ctx;
    }

    public Context getMyContext() {
        return myContext;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.sec_request_quote, container, false);
        myView = rootView;
        myVF = (ViewFlipper) rootView.findViewById(R.id.vfSolicitarCot);
        btnSolicitarCot = (Button) rootView.findViewById(R.id.btnSolicitarCot);

        btnSolicitarCot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (myVF.getCurrentView() == myView.findViewById(R.id.panelSolicitarCotB)) {
                    Toast.makeText(getMyContext(), "Procesando Solicitud de Cotizacion...", Toast.LENGTH_SHORT).show();
                }
                if (myVF.getCurrentView() == myView.findViewById(R.id.panelSolicitarCotA)) {
                    myVF.setInAnimation(getMyContext(), R.anim.slide_in_from_right);
                    myVF.setOutAnimation(getMyContext(), R.anim.slide_out_to_left_2);
                    myVF.showNext();
                }

            }
        });

        return rootView;
    }
}
