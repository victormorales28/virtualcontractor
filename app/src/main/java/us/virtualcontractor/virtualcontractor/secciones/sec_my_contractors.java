package us.virtualcontractor.virtualcontractor.secciones;

import android.app.Fragment;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import us.virtualcontractor.virtualcontractor.R;
import us.virtualcontractor.virtualcontractor.adapter.AdapterContractors;
import us.virtualcontractor.virtualcontractor.db.consult;
import us.virtualcontractor.virtualcontractor.dialog.CallContractor;
import us.virtualcontractor.virtualcontractor.model.MContratista;
import us.virtualcontractor.virtualcontractor.util.AppPreferences;

/**
 * Created by ucweb01 on 15/04/2015.
 */
public class sec_my_contractors extends Fragment{

    private AdapterContractors aContractors;
    private ListView lvContractors;
    private Context ctx;
    private ArrayList<MContratista> oMContractors = new ArrayList<MContratista>();
    public consult oConsult;
    LayoutInflater in;
    ViewGroup con;
    View rootView;
    private AppPreferences loginData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        new LoadData().execute();
        in = inflater;
        con = container;
        rootView = inflater.inflate(R.layout.sec_my_contractors, container, false);
        return rootView;
    }

   class LoadData extends AsyncTask<String,String,ArrayList<MContratista>> {
        @Override
        protected ArrayList<MContratista> doInBackground(String... params) {
            oConsult = new consult();
            loginData = new AppPreferences(con.getContext(),"LoginData");
            String tipo_usuario = loginData.getPreferences("TypeUser","");
            String usuario = loginData.getPreferences("IdUser","");
            oMContractors = oConsult.getMisContractistas("getMyContratistas.php",new String[]{loginData.getPreferences("IdUser","")});
            return oMContractors;
        }

        @Override
        protected void onPostExecute(ArrayList<MContratista> array) {
            super.onPostExecute(array);
            lvContractors = (ListView) rootView.findViewById(R.id.lvContractors);
            aContractors = new AdapterContractors(getActivity(),array);
            lvContractors.setAdapter(aContractors);
            lvContractors.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    CallContractor myModal = new CallContractor(getActivity(),oMContractors.get(position).getTelefono(),oMContractors.get(position).getEmail());
                    myModal.show();

                }
            });
        }
   }
}
