package us.virtualcontractor.virtualcontractor.secciones;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

import us.virtualcontractor.virtualcontractor.R;
import us.virtualcontractor.virtualcontractor.db.consult;
import us.virtualcontractor.virtualcontractor.dialog.CustomAlertDialog;
import us.virtualcontractor.virtualcontractor.interfaces.EventCustomAlertDialog;
import us.virtualcontractor.virtualcontractor.model.MLocation;
import us.virtualcontractor.virtualcontractor.model.MProfesiones;
import us.virtualcontractor.virtualcontractor.searchContractors;

/**
 * Created by ucweb01 on 10/04/2015.
 */
public class sec_find_contractors extends Fragment implements EventCustomAlertDialog{

    private Context ctx;
    private consult oConsult;

    private EditText txt_all_profession;
    private ArrayList<String> listaProfesiones = null;
    private CustomAlertDialog ctmAlertDialog;
    private MProfesiones objProfesiones = null;

    private int indexShowDialog = 0;
    private String[] idsItemsSelected = new String[2];

    private EditText txtCiudad;
    private MLocation patternLocation;
    public ArrayList<String> listaCiudad = null;


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.sec_find_contractors, container, false);
        ctx = container.getContext();
        new LoadData().execute();
        txt_all_profession = (EditText) rootView.findViewById(R.id.txt_all_profession);


        txt_all_profession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_all_profession.setText("");
                if (listaProfesiones != null) {
                    ctmAlertDialog = new CustomAlertDialog(container.getContext(), listaProfesiones, "Select number of weeks.", "multiple");
                    ctmAlertDialog.handlerEvents(sec_find_contractors.this);
                    ctmAlertDialog.setMaxItemsSelected(4);
                    ctmAlertDialog.show();
                    indexShowDialog = 1;
                }
            }
        });
        txtCiudad = (EditText) rootView.findViewById(R.id.txtCiudad);
        txtCiudad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listaCiudad != null) {
                    ctmAlertDialog = new CustomAlertDialog(container.getContext(), listaCiudad, "Select a City.", "single");
                    ctmAlertDialog.handlerEvents(sec_find_contractors.this);
                    ctmAlertDialog.show();
                    indexShowDialog = 2;
                    txtCiudad.setText("");
                }
            }
        });

        Button button = (Button) rootView.findViewById(R.id.searchContractors);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(container.getContext(), searchContractors.class);
                i.putExtra("ids_profesion",idsItemsSelected[0]);
                i.putExtra("id_ciudad",idsItemsSelected[1]);
                startActivity(i);
            }
        });

        return rootView;
    }

    class LoadData extends AsyncTask<String,String,String> {
        @Override
        protected String doInBackground(String... params) {
            oConsult = new consult();
            objProfesiones = oConsult.getProfesiones();
            listaProfesiones = objProfesiones.getListaProfesiones();

            patternLocation = oConsult.getEstadoCiudad();
            listaCiudad = patternLocation.getListaCiudad();

            return null;
        }
    }

    @Override
    public void onADSelectedItem(int index) {
//        Toast.makeText(ctx,"SELECTED ITEM" + index, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onADClickButtonOk() {
        switch (indexShowDialog){
            case 1:
                txt_all_profession.setText(ctmAlertDialog.getStringItemsSelected());
                idsItemsSelected[0] = String.valueOf(objProfesiones.getIDSProfesion(ctmAlertDialog.getIndexItemsSelected()));
                break;
            case 2:
                txtCiudad.setText(ctmAlertDialog.getStringItemSelected());
                idsItemsSelected[1] = String.valueOf(patternLocation.getIDCity(ctmAlertDialog.getIndexItemSelected()));
                break;
        }
    }
}
