package us.virtualcontractor.virtualcontractor.secciones;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import java.util.Calendar;

import us.virtualcontractor.virtualcontractor.ListAlertDialog;
import us.virtualcontractor.virtualcontractor.R;
import us.virtualcontractor.virtualcontractor.dialog.CustomAlertDialog;
import us.virtualcontractor.virtualcontractor.interfaces.EventCustomAlertDialog;
import us.virtualcontractor.virtualcontractor.viewPager.DepthPageTransformer;
import us.virtualcontractor.virtualcontractor.viewPager.MyFragmentPagerAdapter;
import us.virtualcontractor.virtualcontractor.viewPager.ViewPagerFragmentCamera;

//import android.app.Fragment;

/**
 * Created by ucweb01 on 10/04/2015.
 */
public class sec_create_a_project extends Fragment implements EventCustomAlertDialog{

    private Button clickData;
    private ViewPager pager = null;
    public static FragmentManager fgManager;

    private View myView;
    private RelativeLayout btnStepCreateProj;
    private ViewFlipper myVF;
    private TextView txtTitleCreateProjStep;
    private TextView txtNextStepCreateProj;
    private TextView txtEditViewDialog;

    private TextView txtWeekDuration;

    private AlertDialog myAlertDialog;
    private AlertDialog.Builder myAlertBuilder;

    private Context myCtx;

    private CustomAlertDialog ctmAlertDialog;
    private ListAlertDialog myListAlertDialog = new ListAlertDialog();

    private int[] idInputText;
    private String[] msgErrorInputText;

    public sec_create_a_project(FragmentManager fgManager, Context ctx) {
        this.fgManager = fgManager;
        this.myCtx = ctx;
    }

    public Context getMyCtx() {
        return myCtx;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.sec_create_a_project, container, false);
        myView = rootView;
        clickData = (Button)rootView.findViewById(R.id.clickData);
        btnStepCreateProj = (RelativeLayout) rootView.findViewById(R.id.btnStepCreateProj);
        txtTitleCreateProjStep = (TextView) rootView.findViewById(R.id.txtTitleCreateProjStep);
        txtNextStepCreateProj = (TextView) rootView.findViewById(R.id.txtNextStepCreateProj);
        txtWeekDuration = (TextView) rootView.findViewById(R.id.txtWeekDuration);

        myVF = (ViewFlipper) rootView.findViewById(R.id.vfCreateProject);
        myVF.setInAnimation(getMyCtx(), R.anim.slide_in_from_right);
        myVF.setOutAnimation(getMyCtx(), R.anim.slide_out_to_left_2);

        myAlertBuilder = new AlertDialog.Builder(myCtx);

        idInputText = new int[]{R.id.txtProjectName, R.id.txtProjectDescription};
        msgErrorInputText = new String[]{"Project's name is required!", "Project Description is required!"};

        btnStepCreateProj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (myVF.getCurrentView() == myView.findViewById(R.id.panelCreateProjC)) {
                    Toast.makeText(getMyCtx(),"Step 3",Toast.LENGTH_SHORT).show();
                }
                if (myVF.getCurrentView() == myView.findViewById(R.id.panelCreateProjB)) {
                    myVF.showNext();
                    txtTitleCreateProjStep.setText("STEP 3");
                    txtNextStepCreateProj.setText("CREATE PROJECT");
                }
                if (myVF.getCurrentView() == myView.findViewById(R.id.panelCreateProjA) && !validateIsEmptyInputs(-1)) {
                    myVF.showNext();
                    txtTitleCreateProjStep.setText("STEP 2");
                    txtNextStepCreateProj.setText("SAVE & STEP 3");
                }
            }
        });
        clickData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment = new SelectDateFragment();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        txtWeekDuration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ctmAlertDialog = new CustomAlertDialog(getMyCtx(), myListAlertDialog.listaSemanasProyectos, "How many weeks will it take you to finish the Project?", "single");
                ctmAlertDialog.handlerEvents(sec_create_a_project.this);
                ctmAlertDialog.show();
            }
        });


        this.pager = (ViewPager) rootView.findViewById(R.id.pager);
        this.pager.setPageTransformer(true, new DepthPageTransformer());
        MyFragmentPagerAdapter adapter = new MyFragmentPagerAdapter(this.fgManager,container.getContext());
        for (int i = 0; i <4; i++) {
            adapter.addFragment(ViewPagerFragmentCamera.newInstance());
            this.pager.setAdapter(adapter);
        }
        return rootView;
    }

    @Override
    public void onADSelectedItem(int index) {
        Toast.makeText(getMyCtx(), "Selected Item " + index, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onADClickButtonOk() {
        ctmAlertDialog.close();
    }


    public class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener
    {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar calendar = Calendar.getInstance();
            int yy = calendar.get(Calendar.YEAR);
            int mm = calendar.get(Calendar.MONTH);
            int dd = calendar.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getActivity(), this, yy, mm, dd);
        }

        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            populateSetDate(yy, mm+1, dd);
        }
        public void populateSetDate(int year, int month, int day) {
            clickData.setText(month+"/"+day+"/"+year);
        }
    }

    private Boolean validateIsEmptyInputs(int validateInput) {

        Boolean validate = false;
        EditText txt;

        for (int i = 0; i < idInputText.length; i++) {
            if (validateInput != -1 && validateInput != idInputText[i]) continue;
            txt = (EditText) myView.findViewById(idInputText[i]);
            if (txt.getText().toString().trim().length() == 0) {
                txt.setError(msgErrorInputText[i]);
                validate = true;
            }
        }

        return validate;
    }

}