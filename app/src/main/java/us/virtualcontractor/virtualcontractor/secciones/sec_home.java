package us.virtualcontractor.virtualcontractor.secciones;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import us.virtualcontractor.virtualcontractor.MainApp;
import us.virtualcontractor.virtualcontractor.R;

;

/**
 * Created by ucweb01 on 20/04/2015.
 */
public class sec_home extends Fragment {
    //private ListView mDrawerList;
    //private DrawerLayout mDrawerLayout;
//    Context ctx;
    MainApp mainAct;

    public sec_home(Context ctx) {
//        this.ctx = ctx;
        this.mainAct = (MainApp)ctx;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.sec_home, container, false);

//        final MainApp oMain = new MainApp();
        final ImageButton my_profile = (ImageButton) rootView.findViewById(R.id.imgb_my_profile);
        my_profile.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mainAct.displayView(0);
            }
        });
        final TextView txt_my_profile = (TextView) rootView.findViewById(R.id.txt_my_profile);
        txt_my_profile.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mainAct.displayView(0);
            }
        });

        final ImageButton my_proyects = (ImageButton) rootView.findViewById(R.id.imgb_my_proyects);
        my_proyects.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mainAct.displayView(1);
            }
        });
        final TextView txt_my_proyects = (TextView) rootView.findViewById(R.id.txt_my_proyects);
        txt_my_proyects.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mainAct.displayView(1);
            }
        });

        final ImageButton create_a_project = (ImageButton) rootView.findViewById(R.id.imgb_create_a_project);
        create_a_project.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mainAct.displayView(2);
            }
        });
        final TextView txt_create_a_project= (TextView) rootView.findViewById(R.id.txt_create_a_project);
        txt_create_a_project.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mainAct.displayView(2);
            }
        });

        final ImageButton find_contractors = (ImageButton) rootView.findViewById(R.id.imgb_find_contractors);
        find_contractors.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mainAct.displayView(3);
            }
        });
        final TextView txt_find_contractors= (TextView) rootView.findViewById(R.id.txt_find_contractors);
        txt_find_contractors.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mainAct.displayView(3);
            }
        });

        final ImageButton my_contractos = (ImageButton) rootView.findViewById(R.id.imgb_my_contractos);
        my_contractos.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mainAct.displayView(4);
            }
        });
        final TextView txt_my_contractos= (TextView) rootView.findViewById(R.id.txt_my_contractos);
        txt_my_contractos.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mainAct.displayView(4);
            }
        });

        final ImageButton request_quote = (ImageButton) rootView.findViewById(R.id.imgb_request_quote);
        request_quote.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mainAct.displayView(5);
            }
        });
        final TextView txt_request_quote= (TextView) rootView.findViewById(R.id.txt_request_quote);
        txt_request_quote.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mainAct.displayView(5);
            }
        });

        return rootView;
    }
}
