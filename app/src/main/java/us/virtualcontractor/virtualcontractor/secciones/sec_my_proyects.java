package us.virtualcontractor.virtualcontractor.secciones;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import us.virtualcontractor.virtualcontractor.R;
import us.virtualcontractor.virtualcontractor.adapter.AdapterPerfilContratista;
import us.virtualcontractor.virtualcontractor.db.consult;
import us.virtualcontractor.virtualcontractor.detailProject;
import us.virtualcontractor.virtualcontractor.model.MPerfilContratista;
import us.virtualcontractor.virtualcontractor.model.MProyectos;
import us.virtualcontractor.virtualcontractor.util.AppPreferences;


/**
 * Created by ucweb01 on 10/04/2015.
 */
public class sec_my_proyects extends Fragment {

    private ArrayList<MProyectos> mProyecto;
    private AdapterPerfilContratista aPerfilContratista;
    private ListView lvProyects;

    private Context ctx;
    private consult oConsult;
    private ArrayList<MPerfilContratista> oMPerfilContratista = new ArrayList<MPerfilContratista>();
    private AppPreferences loginData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.sec_my_projects, container, false);

        lvProyects = (ListView)rootView.findViewById(R.id.lvProyects);
        ctx = container.getContext();
        if (oMPerfilContratista.size()==0){
            new LoadData().execute();
        }
        else{
            mostrarListView(oMPerfilContratista);
        }
        /*aProyectos = new AdapterProyectos(getActivity(), generateData());
        ctx = aProyectos.getContext();
        lvProyects = (ListView) rootView.findViewById(R.id.lvProyects);
        lvProyects.setAdapter(aProyectos);

        lvProyects.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(ctx, detailProject.class);
                i.putExtra("tipo","proyecto");
                i.putExtra("codigo", mProyecto.get(position).getNombre());
                startActivity(i);
                ((Activity) ctx).overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
            }
        });*/

        return rootView;
    }

    class LoadData extends AsyncTask<String,String,ArrayList<MPerfilContratista>> {
        @Override
        protected ArrayList<MPerfilContratista> doInBackground(String... params) {
            loginData = new AppPreferences(ctx,"LoginData");
            oConsult = new consult();
            oMPerfilContratista = oConsult.getMyProyectos(loginData.getPreferences("IdUser",""),loginData.getPreferences("TypeUser",""));
            return oMPerfilContratista;
        }

        @Override
        protected void onPostExecute(ArrayList<MPerfilContratista> array){
            super.onPostExecute(array);
            mostrarListView(array);
        }
    }

    private void mostrarListView(ArrayList<MPerfilContratista> array){
        aPerfilContratista = new AdapterPerfilContratista(ctx,array,1);
        lvProyects.setAdapter(aPerfilContratista);
        lvProyects.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(ctx, detailProject.class);
                i.putExtra("tipo","proyecto");
                i.putExtra("id_proyecto", oMPerfilContratista.get(position).getId_proyecto());
                startActivity(i);
//                ((Activity) ctx).overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
            }
        });
    }
}
