package us.virtualcontractor.virtualcontractor.secciones;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import us.virtualcontractor.virtualcontractor.R;

/**
 * Created by ucweb01 on 29/05/2015.
 */
public class sec_contractor_blog extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.sec_contractor_blog, container, false);
        return rootView;
    }
}
