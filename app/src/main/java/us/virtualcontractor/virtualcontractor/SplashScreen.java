package us.virtualcontractor.virtualcontractor;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import us.virtualcontractor.virtualcontractor.db.consult;
import us.virtualcontractor.virtualcontractor.model.MUsuario;
import us.virtualcontractor.virtualcontractor.util.AppPreferences;

public class SplashScreen extends Activity {

    private View uiApp;
    private ProgressBar mProgress;
    private int mProgressStatus = 0;
    Handler mHideHandler = new Handler();

    private consult oConsult;
    private MUsuario objUsuario = null;
    private AppPreferences shrPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        shrPref = new AppPreferences(SplashScreen.this, "LoginData");

        new LoadData().execute(shrPref.getPreferences("UserName", ""), shrPref.getPreferences("PassWord", ""));

        uiApp = getWindow().getDecorView();
        hideSystemUI();

        mProgress = (ProgressBar) findViewById(R.id.progressBar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_splash_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class LoadData extends AsyncTask<String, String, MUsuario>{
        @Override
        protected MUsuario doInBackground(String... params) {
            oConsult = new consult();
            objUsuario = oConsult.login(params[0], params[1]);
            return objUsuario;
        }

        @Override
        protected void onPostExecute(MUsuario tipoUsuario) {
            super.onPostExecute(tipoUsuario);
            validarLogin(tipoUsuario);
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }
    }

    Runnable runValidateLogin = new Runnable() {
        @Override
        public void run() {
//            Intent i;
//            if (shrPref.getString("UserName", "") == "" && shrPref.getString("PassWord", "") == "") {
//                i = new Intent(getApplicationContext(), HomeApp.class);
//            } else {
//                if (tipoUsuario == "") {
//                    i = new Intent(getApplicationContext(), HomeApp.class);
//                } else {
//                    i = new Intent(getApplicationContext(), MainApp.class);
//                }
//            }
//            showSystemUI();
//            startActivity(i);
//            finish();
        }
    };

    private void validarLogin(MUsuario mUsuario){
        Intent i;
        if (mUsuario == null) {
            i = new Intent(getApplicationContext(), HomeApp.class);
        } else {
            if (mUsuario == null) {
                i = new Intent(getApplicationContext(), HomeApp.class);
            } else {
                i = new Intent(getApplicationContext(), MainApp.class);
                i.putExtra("TypeUser", shrPref.getPreferences("TypeUser", ""));
            }
        }
        showSystemUI();
        startActivity(i);
        finish();
    }

    Runnable runLoadApp = new Runnable() {
        @Override
        public void run() {
            while (mProgressStatus < 100) {
                mProgressStatus++;
                // Update the progress bar
                mHideHandler.post(new Runnable() {
                    public void run() {
                        mProgress.setProgress(mProgressStatus);
                    }
                });
            }
        }
    };

    // This snippet hides the system bars.
    private void hideSystemUI() {
        // Set the IMMERSIVE flag.
        // Set the content to appear under the system bars so that the content
        // doesn't resize when the system bars hide and show.
        uiApp.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    private void showSystemUI() {
        uiApp.setSystemUiVisibility(View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

//        // Start lengthy operation in a background thread
//        new Thread(new Runnable() {
//            public void run() {
//                while (mProgressStatus < 100) {
//                    mProgressStatus++;
//                    // Update the progress bar
//                    mHideHandler.post(new Runnable() {
//                        public void run() {
//                            mProgress.setProgress(mProgressStatus);
//                        }
//                    });
//                }
//                loadLogin(1000);
//            }
//        }).start();
        mHideHandler.postDelayed(runLoadApp, 1000);
    }
}
