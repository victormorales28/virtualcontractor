package us.virtualcontractor.virtualcontractor;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import us.virtualcontractor.virtualcontractor.adapter.AdapterPerfilContratista;
import us.virtualcontractor.virtualcontractor.db.consult;
import us.virtualcontractor.virtualcontractor.dialog.AddContractor;
import us.virtualcontractor.virtualcontractor.model.MPerfilContratista;

public class detailContractor extends ActionBarActivity {


    private AdapterPerfilContratista aPerfilContratista;
    private ListView lvPerfilContratista;
    private Context ctx;

    private consult oConsult;
    private String id_contratista;
    private static ArrayList<MPerfilContratista> oMPerfilContratista = new ArrayList<MPerfilContratista>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_contractor);

        ctx = getApplicationContext();
        Bundle bundle = getIntent().getExtras();
        id_contratista = bundle.getString("id");
        lvPerfilContratista = (ListView) findViewById(R.id.lvProyects);

        if(oMPerfilContratista.size()==0){
            new LoadData().execute();
        }
        else{
            aPerfilContratista = new AdapterPerfilContratista(ctx,oMPerfilContratista);
            lvPerfilContratista.setAdapter(aPerfilContratista);
            lvPerfilContratista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if(position == 0){
                        AddContractor myModal = new AddContractor(detailContractor.this,String.valueOf(oMPerfilContratista.get(position).getId_contratista()),oMPerfilContratista.get(position).getNombre() + " " + oMPerfilContratista.get(position).getApellido());
                        myModal.show();
                    }
                    else{
                        Intent i = new Intent(ctx, detailSearchProject.class);
                        i.putExtra("id_proyecto", oMPerfilContratista.get(position).getId_proyecto());
                        startActivity(i);
                    }
                }
            });
        }
    }

    class LoadData extends AsyncTask<String,String,ArrayList<MPerfilContratista>> {
        @Override
        protected ArrayList<MPerfilContratista> doInBackground(String... params) {
            oConsult = new consult();
            oMPerfilContratista = oConsult.perfilContractistas(id_contratista);
            return oMPerfilContratista;
        }

        @Override
        protected void onPostExecute(ArrayList<MPerfilContratista> array){
            super.onPostExecute(array);
            aPerfilContratista = new AdapterPerfilContratista(ctx,array);
            lvPerfilContratista.setAdapter(aPerfilContratista);
            lvPerfilContratista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if(position == 0){
                        AddContractor myModal = new AddContractor(detailContractor.this,String.valueOf(oMPerfilContratista.get(position).getId_contratista()),oMPerfilContratista.get(position).getNombre() + " " + oMPerfilContratista.get(position).getApellido());
                        myModal.show();
                    }
                    else{
                        Intent i = new Intent(ctx, detailSearchProject.class);
                        i.putExtra("id_proyecto", oMPerfilContratista.get(position).getId_proyecto());
                        startActivity(i);
                    }
                }
            });
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_detail_contractor, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                oMPerfilContratista = new ArrayList<MPerfilContratista>();
                return true;
            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        oMPerfilContratista = new ArrayList<MPerfilContratista>();
    }
}
