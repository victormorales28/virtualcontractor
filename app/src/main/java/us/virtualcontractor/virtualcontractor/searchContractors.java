package us.virtualcontractor.virtualcontractor;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import us.virtualcontractor.virtualcontractor.adapter.AdapterContractors;
import us.virtualcontractor.virtualcontractor.db.consult;
import us.virtualcontractor.virtualcontractor.model.MContratista;


public class searchContractors extends ActionBarActivity {

    private AdapterContractors aContractors;
    private ListView lvContractors;
    private Context ctx;

    public consult oConsult;
    private String ids_profesion;
    private String id_ciudad;
    private static ArrayList<MContratista> oMContractors = new ArrayList<MContratista>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_contractors);

        Bundle bundle = getIntent().getExtras();
        ids_profesion = bundle.getString("ids_profesion");
        id_ciudad = bundle.getString("id_ciudad");
        ctx = getApplicationContext();
        lvContractors = (ListView) findViewById(R.id.lvContractors);

        if(oMContractors.size()==0){
            new LoadData().execute();
        }
        else{
            aContractors = new AdapterContractors(ctx,oMContractors);
            lvContractors.setAdapter(aContractors);
            lvContractors.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent i = new Intent(ctx, detailContractor.class);
                    i.putExtra("id", String.valueOf(oMContractors.get(position).getId()));
                    startActivity(i);
                }
            });
        }
    }

    class LoadData extends AsyncTask<String,String,ArrayList<MContratista>> {
        @Override
        protected ArrayList<MContratista> doInBackground(String... params) {
            oConsult = new consult();
            oMContractors = oConsult.getMisContractistas("buscarContratistas.php", new String[]{ids_profesion, id_ciudad});
            return oMContractors;
        }

        @Override
        protected void onPostExecute(ArrayList<MContratista> array) {
            super.onPostExecute(array);
            aContractors = new AdapterContractors(ctx,array);
            lvContractors.setAdapter(aContractors);
            lvContractors.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent i = new Intent(ctx, detailContractor.class);
                    i.putExtra("id", String.valueOf(oMContractors.get(position).getId()));
                    startActivity(i);
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search_contractors, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                oMContractors = new ArrayList<MContratista>();
                return true;
            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        oMContractors = new ArrayList<MContratista>();
    }
}
