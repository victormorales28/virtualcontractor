package us.virtualcontractor.virtualcontractor;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import us.virtualcontractor.virtualcontractor.adapter.AdapterPerfilProyecto;
import us.virtualcontractor.virtualcontractor.db.consult;
import us.virtualcontractor.virtualcontractor.model.MPerfilProyecto;


public class detailSearchProject extends ActionBarActivity {

    private Context ctx;
    private consult oConsult;
    private static ArrayList<MPerfilProyecto> oMPerfilProyecto = new ArrayList<MPerfilProyecto>();
    private String id_proyecto;
    private AdapterPerfilProyecto aPerfilProyecto;
    private ListView lvPerfilProyecto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_search_project);

        Bundle bundle = getIntent().getExtras();
        id_proyecto = bundle.getString("id_proyecto");

        lvPerfilProyecto = (ListView) findViewById(R.id.lvWeeks);
        ctx = getApplicationContext();

        if(oMPerfilProyecto.size()==0){
            new LoadData().execute();
        }
        else{
            mostrarListView(oMPerfilProyecto);
            /*aPerfilProyecto = new AdapterPerfilProyecto(ctx,oMPerfilProyecto);
            lvPerfilProyecto.setAdapter(aPerfilProyecto);
            lvPerfilProyecto.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if(position != 0){
                        Intent i = new Intent(ctx, detailProject.class);
                        i.putExtra("tipo","semana");
                        i.putExtra("nombre_proyecto", oMPerfilProyecto.get(0).getNombre_proyecto());
                        i.putExtra("id_semana", oMPerfilProyecto.get(position).getId_semana());
                        i.putExtra("numero_semana", oMPerfilProyecto.get(position).getNumero_semana());
                        startActivity(i);
                    }
                }
            });*/
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_detail_search_project, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                oMPerfilProyecto = new ArrayList<MPerfilProyecto>();
                return true;
            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        oMPerfilProyecto = new ArrayList<MPerfilProyecto>();
    }

    class LoadData extends AsyncTask<String,String,ArrayList<MPerfilProyecto>> {
        @Override
        protected ArrayList<MPerfilProyecto> doInBackground(String... params) {
            oConsult = new consult();
            oMPerfilProyecto = oConsult.perfilProyecto(id_proyecto);
            return oMPerfilProyecto;
        }

        @Override
        protected void onPostExecute(ArrayList<MPerfilProyecto> array){
            super.onPostExecute(array);
            mostrarListView(array);
        }
    }

    private void mostrarListView(ArrayList<MPerfilProyecto> array){
        aPerfilProyecto = new AdapterPerfilProyecto(ctx,array);
        lvPerfilProyecto.setAdapter(aPerfilProyecto);
        lvPerfilProyecto.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(position != 0){
                    Intent i = new Intent(ctx, detailProject.class);
                    i.putExtra("tipo","semana");
                    i.putExtra("nombre_proyecto", oMPerfilProyecto.get(0).getNombre_proyecto());
                    i.putExtra("id_semana", oMPerfilProyecto.get(position).getId_semana());
                    i.putExtra("numero_semana", oMPerfilProyecto.get(position).getNumero_semana());
                    startActivity(i);
                }
            }
        });
    }
}
