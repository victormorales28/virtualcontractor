package us.virtualcontractor.virtualcontractor;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.Toast;

/**
 * Created by ucweb02 on 15/04/2015.
 */
public class CustomAlertDialog {

    private AlertDialog levelDialog;
    private AlertDialog.Builder builder;
    private String titleDialog;
    private CharSequence[] items;
    private Context activityContext;
    private int indexItemSelected = 0;

    public CustomAlertDialog(Context ctx, CharSequence[] myItems, String myTitle) {
        this.activityContext = ctx;
        this.items = myItems;
        this.titleDialog = myTitle;
    }

    public void showDialog(){
        this.builder = new AlertDialog.Builder(activityContext);
        this.builder.setTitle(this.titleDialog);

        this.builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                setIndexItemSelected(item);
                Toast.makeText(activityContext, "Selecciono " + items[item], Toast.LENGTH_SHORT).show();
                levelDialog.dismiss();
            }
        });

        this.levelDialog = builder.create();
        this.levelDialog.show();
    }

    public int getIndexItemSelected() {
        return indexItemSelected;
    }

    public void setIndexItemSelected(int indexItemSelected) {
        this.indexItemSelected = indexItemSelected;
    }
}
