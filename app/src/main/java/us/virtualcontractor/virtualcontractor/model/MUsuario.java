package us.virtualcontractor.virtualcontractor.model;

/**
 * Created by ucweb01 on 07/05/2015.
 */
public class MUsuario extends MCiudad{
    private int id_usuario;
    private String nombre;
    private String contrasenia;
    private int tipousuario;
    private String fecha_logueo;

    public MUsuario(int id_usuario, String nombre, String contrasenia, int tipousuario) {
        this.id_usuario = id_usuario;
        this.nombre = nombre;
        this.contrasenia = contrasenia;
        this.tipousuario = tipousuario;
    }

    public int getId_usuario() {
        return id_usuario;
    }
    public void setId_usuario( int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getContrasenia() {
        return contrasenia;
    }
    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    public String getFecha_logueo() {
        return fecha_logueo;
    }
    public void setFecha_logueo(String fecha_logueo) {
        this.fecha_logueo = fecha_logueo;
    }

    public int getTipousuario() {
        return tipousuario;
    }
    public void setTipousuario(int tipousuario) {
        this.tipousuario = tipousuario;
    }

}
