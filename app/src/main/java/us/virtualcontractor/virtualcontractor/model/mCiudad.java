package us.virtualcontractor.virtualcontractor.model;

/**
 * Created by ucweb01 on 06/05/2015.
 */
public class MCiudad extends MEstado{
    private int id_ciudad;
    private String nombre_ciudad;
    private int id_estado;

    public int getId_ciudad() {
        return id_ciudad;
    }
    public void setId_ciudad(int id_ciudad) {
        this.id_ciudad = id_ciudad;
    }

    public String getNombre_ciudad() {
        return nombre_ciudad;
    }
    public void setNombre_ciudad(String nombre_ciudad) {
        this.nombre_ciudad = nombre_ciudad;
    }

    public int getId_estado() {
        return id_estado;
    }
    public void setId_estado(int id_estado) {
        this.id_estado = id_estado;
    }
}
