package us.virtualcontractor.virtualcontractor.model;

import java.util.ArrayList;

/**
 * Created by ucweb01 on 22/05/2015.
 */
public class MPerfilSemana {

    private String titulo;
    private String avance;
    private String video;
    private ArrayList<String> foto = new ArrayList<String>();

    public MPerfilSemana(String titulo, String avance, String video) {
        this.titulo = titulo;
        this.avance = avance;
        this.video = video;
    }

    public String getTitulo() {
        return titulo;
    }
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAvance() {
        return avance;
    }
    public void setAvance(String avance) {
        this.avance = avance;
    }

    public String getVideo() {
        return video;
    }
    public void setVideo(String video) {
        this.video = video;
    }

    public ArrayList<String> getFoto() {
        return foto;
    }
    public void setFoto(String foto) {
        this.foto.add(foto);
    }
}
