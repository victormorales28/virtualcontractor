package us.virtualcontractor.virtualcontractor.model;

import java.util.ArrayList;

/**
 * Created by ucweb02 on 28/05/2015.
 */
public class MPerfilUsuario {

    private String nombre = "";
    private String apellido = "";
    private String compania = "";
    private String telefono = "";
    private String movil = "";
    private Integer id_estado = 0;
    private Integer id_ciudad = 0;
    private String email = "";
    private String avatar = "";
    private ArrayList<Integer> profesion = new ArrayList<Integer>();

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCompania() {
        return compania;
    }

    public void setCompania(String compania) {
        this.compania = compania;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getMovil() {
        return movil;
    }

    public void setMovil(String movil) {
        this.movil = movil;
    }

    public Integer getId_estado() {
        return id_estado;
    }

    public void setId_estado(Integer id_estado) {
        this.id_estado = id_estado;
    }

    public Integer getId_ciudad() {
        return id_ciudad;
    }

    public void setId_ciudad(Integer id_ciudad) {
        this.id_ciudad = id_ciudad;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public ArrayList<Integer> getProfesion() {
        return profesion;
    }

    public void setProfesion(ArrayList<Integer> profesion) {
        this.profesion = profesion;
    }

}
