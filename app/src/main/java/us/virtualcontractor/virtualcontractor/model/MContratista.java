package us.virtualcontractor.virtualcontractor.model;

/**
 * Created by ucweb01 on 24/04/2015.
 */
public class MContratista extends MUsuario{

    private int id;
    private String nombre;
    private String apellido;
    private String compania;
    private String telefono;
    private String movil;
    private String email;
    private String avatar;
    private String profesion;

    public MContratista(String nombre_contratista, int id_usuario, String nombre, String contrasenia, int tipousuario){
        super(id_usuario, nombre, contrasenia, tipousuario);
        this.id = id_usuario;
        this.nombre = nombre_contratista;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre_contractor) {
        this.nombre = nombre_contractor;
    }

    public String getApellido() {
        return apellido;
    }
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCompania() {
        return compania;
    }
    public void setCompania(String compania) {
        this.compania = compania;
    }

    public String getTelefono() {
        return telefono;
    }
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getMovil() {
        return movil;
    }
    public void setMovil(String movil) {
        this.movil = movil;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String getAvatar() {
        return avatar;
    }
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getProfesion() {
        return profesion;
    }
    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }
}
