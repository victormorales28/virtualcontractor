package us.virtualcontractor.virtualcontractor.model;

import java.util.ArrayList;

/**
 * Created by ucweb02 on 12/05/2015.
 */
public class MProfesiones {

    private ArrayList<String> listaProfesiones = new ArrayList<String>();
    private ArrayList<MProfesion> objProfesiones;

    public MProfesiones(ArrayList<MProfesion> objProfesiones) {
        this.objProfesiones = objProfesiones;
    }

    public ArrayList<String> getListaProfesiones() {
        MProfesion mp;
        for (int i = 0; i < objProfesiones.size(); i++){
            mp = objProfesiones.get(i);
            listaProfesiones.add(mp.getNombre());
        }
        return listaProfesiones;
    }

    public int getIDProfesion(int index){
        return  objProfesiones.get(index).getId();
    }

    public String getIDSProfesion(ArrayList<Integer> indexs){
        String ids = "";
        MProfesion mp;
        for (int i=0; i < indexs.size() ; i++){
            mp = objProfesiones.get(indexs.get(i));
            ids = ids + mp.getId().toString() + ",";
        }
        return  ids;
    }

    public String getTextProfesiones(ArrayList<Integer> indexs){
        MProfesion mp;
        String profesiones = "";

        for (int i = 0; i < objProfesiones.size(); i++){
            mp = objProfesiones.get(i);
            for (int j = 0; j < indexs.size(); j++) {
                if (mp.getId() == indexs.get(j)) {
                    profesiones += mp.getNombre() + ", ";
                }
            }
        }

        if (profesiones.length() > 0) {
            profesiones = profesiones.substring(0, profesiones.length() - 2);
        }

        return profesiones;
    }
}









