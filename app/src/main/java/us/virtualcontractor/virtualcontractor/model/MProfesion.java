package us.virtualcontractor.virtualcontractor.model;

/**
 * Created by ucweb02 on 12/05/2015.
 */
public class MProfesion {
    private int id = 0;
    private String nombre = "";

    public MProfesion(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
