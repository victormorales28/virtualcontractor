package us.virtualcontractor.virtualcontractor.model;

/**
 * Created by ucweb01 on 27/04/2015.
 */
public class MWeek {
    //private int header;
    private String nombre;

    /*public ModelWeek(String nombre,int header){
        super();
        this.nombre = nombre;
        this.header = header;
    }*/

    public MWeek(String nombre){
        super();
        this.nombre = nombre;
        //this.header=0;
    }

    /*public int getHeader() {
        return header;
    }
    public void setHeader(int header) {
        this.header = header;
    }*/

    public String  getNombre() {
        return nombre;
    }
    public void setNombre(String  nombre) {
        this.nombre = nombre;
    }
}
