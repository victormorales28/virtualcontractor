package us.virtualcontractor.virtualcontractor.model;

/**
 * Created by ucweb01 on 18/05/2015.
 */
public class MPerfilContratista {

    private int id_contratista;
    private String nombre;
    private String apellido;
    private String compania;
    private String telefono;
    private String movil;
    private String email;
    private String avatar;
    private String nombre_ciudad;
    private String nombre_estado;
    private String profesion;

    public MPerfilContratista(int id_contratista, String nombre, String apellido, String compania, String telefono, String movil, String email, String avatar, String nombre_ciudad, String nombre_estado, String profesion) {
        this.id_contratista = id_contratista;
        this.nombre = nombre;
        this.apellido = apellido;
        this.compania = compania;
        this.telefono = telefono;
        this.movil = movil;
        this.email = email;
        this.avatar = avatar;
        this.nombre_ciudad = nombre_ciudad;
        this.nombre_estado = nombre_estado;
        this.profesion = profesion;
    }

    public int getId_contratista() {
        return id_contratista;
    }
    public void setId_contratista(int id_contratista) {
        this.id_contratista = id_contratista;
    }

    public String getNombre_estado() {
        return nombre_estado;
    }
    public void setNombre_estado(String nombre_estado) {
        this.nombre_estado = nombre_estado;
    }

    public String getNombre_ciudad() {
        return nombre_ciudad;
    }
    public void setNombre_ciudad(String nombre_ciudad) {
        this.nombre_ciudad = nombre_ciudad;
    }

    public String getAvatar() {
        return avatar;
    }
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String getMovil() {
        return movil;
    }
    public void setMovil(String movil) {
        this.movil = movil;
    }

    public String getTelefono() {
        return telefono;
    }
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCompania() {
        return compania;
    }
    public void setCompania(String compania) {
        this.compania = compania;
    }

    public String getApellido() {
        return apellido;
    }
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getProfesion() {
        return profesion;
    }
    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }

    private String id_proyecto;
    private String nombre_proyecto;
    private String sub_titulo;
    private Double valoracion;
    private String foto_proyecto;

    public MPerfilContratista(String id_proyecto, String nombre_proyecto, String sub_titulo, Double valoracion, String foto_proyecto) {
        this.id_proyecto = id_proyecto;
        this.nombre_proyecto = nombre_proyecto;
        this.sub_titulo = sub_titulo;
        this.valoracion = valoracion;
        this.foto_proyecto = foto_proyecto;
    }

    public String getId_proyecto() {
        return id_proyecto;
    }
    public void setId_proyecto(String id_proyecto) {
        this.id_proyecto = id_proyecto;
    }

    public String getNombre_proyecto() {
        return nombre_proyecto;
    }
    public void setNombre_proyecto(String nombre_proyecto) {
        this.nombre_proyecto = nombre_proyecto;
    }

    public String getSubTitulo() {
        return sub_titulo;
    }
    public void setSubTitulo(String sub_titulo) {
        this.sub_titulo = sub_titulo;
    }

    public Double getValoracion() {
        return valoracion;
    }
    public void setValoracion(Double valoracion) {
        this.valoracion = valoracion;
    }

    public String getFoto_proyecto() {
        return foto_proyecto;
    }
    public void setFoto_proyecto(String foto_proyecto) {
        this.foto_proyecto = foto_proyecto;
    }
}














