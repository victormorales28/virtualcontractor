package us.virtualcontractor.virtualcontractor.model;

import java.util.ArrayList;

/**
 * Created by ucweb01 on 20/05/2015.
 */
public class MPerfilProyecto {

    private String nombre_proyecto;
    private String sub_titulo;
    private String descripcion;
    private int total_semanas;
    private String fecha_inicio;
    private ArrayList<String> foto = new ArrayList<String>();

    public MPerfilProyecto(String nombre_proyecto, String sub_titulo, String descripcion, int total_semanas, String fecha_inicio) {
        this.nombre_proyecto = nombre_proyecto;
        this.sub_titulo = sub_titulo;
        this.descripcion = descripcion;
        this.total_semanas = total_semanas;
        this.fecha_inicio = fecha_inicio;
    }

    public String getNombre_proyecto() {
        return nombre_proyecto;
    }
    public void setNombre_proyecto(String nombre_proyecto) {
        this.nombre_proyecto = nombre_proyecto;
    }

    public String getSub_titulo() {
        return sub_titulo;
    }
    public void setSub_titulo(String sub_titulo) {
        this.sub_titulo = sub_titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getTotal_semanas() {
        return total_semanas;
    }
    public void setTotal_semanas(int total_semanas) {
        this.total_semanas = total_semanas;
    }

    public String getFecha_inicio() {
        return fecha_inicio;
    }
    public void setFecha_inicio(String fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    public ArrayList<String> getFoto() {
        return foto;
    }
    public void setFoto(String foto) {
        this.foto.add(foto);
    }

    private String id_semana;
    private String numero_semana;

    public MPerfilProyecto(String id_semana, String numero_semana) {
        this.id_semana = id_semana;
        this.numero_semana = numero_semana;
    }

    public String getId_semana() {
        return id_semana;
    }
    public void setId_semana(String id_semana) {
        this.id_semana = id_semana;
    }

    public String getNumero_semana() {
        return numero_semana;
    }
    public void setNumero_semana(String numero_semana) {
        this.numero_semana = numero_semana;
    }

}
