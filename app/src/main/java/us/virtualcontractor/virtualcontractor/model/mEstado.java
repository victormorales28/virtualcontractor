package us.virtualcontractor.virtualcontractor.model;

/**
 * Created by ucweb01 on 06/05/2015.
 */
public class MEstado {

    private int id_estado;
    private String nombre_estado;

    public String getNombre_estado() {
        return nombre_estado;
    }
    public void setNombre_estado(String nombre_estado) {
        this.nombre_estado = nombre_estado;
    }

    public int getId_estado() {
        return id_estado;
    }
    public void setId_estado(int id_estado) {
        this.id_estado = id_estado;
    }
}
