package us.virtualcontractor.virtualcontractor.model;

import java.util.ArrayList;

/**
 * Created by ucweb01 on 06/05/2015.
 */
public class MLocation {

    private ArrayList<String> listaEstado;
    private ArrayList<String> listaCiudad;
    private ArrayList<MEstado> mEstado;
    private ArrayList<MCiudad> mCiudad;

    public MLocation(ArrayList<MEstado> me, ArrayList<MCiudad> mc) {
        mEstado = me;
        mCiudad = mc;
    }

    public ArrayList<String> getListaEstados() {
        listaEstado = new ArrayList<String>();
        for (int i = 0; i < mEstado.size(); i++) {
            MEstado me = mEstado.get(i);
            listaEstado.add(me.getNombre_estado());
        }
        return listaEstado;
    }

    public ArrayList<String> getListaCiudadPorEstado(int index) {
        MCiudad mc;
        MEstado me;
        listaCiudad = new ArrayList<String>();
        for (int i = 0; i < mCiudad.size(); i++) {
            mc = mCiudad.get(i);
            me = mEstado.get(index);
            if (mc.getId_estado() == me.getId_estado()) {
                listaCiudad.add(mc.getNombre_ciudad());
            }
        }
        return listaCiudad;
    }

    public ArrayList<String> getListaCiudad() {
        listaCiudad = new ArrayList<String>();
        for (int i = 0; i < mCiudad.size(); i++) {
            MCiudad me = mCiudad.get(i);
            listaCiudad.add(me.getNombre_ciudad());
        }
        return listaCiudad;
    }

    public int getIDState(int index) {
        return mEstado.get(index).getId_estado();
    }

    public int getIDCity(int index) {
        return mCiudad.get(index).getId_ciudad();
    }

    public String getTextStateByID(int id){
        MEstado me;
        for (int i = 0; i < mEstado.size(); i++) {
            me = mEstado.get(i);
            if (me.getId_estado() == id) {
                return me.getNombre_estado();
            }
        }
        return "No State.";
    }

    public String getTextCityByID(int id){
        MCiudad mc;
        for (int i = 0; i < mCiudad.size(); i++) {
            mc = mCiudad.get(i);
            if (mc.getId_ciudad() == id) {
                return mc.getNombre_ciudad();
            }
        }
        return "No City.";
    }

    public Integer getIDCityByText(String value){
        MCiudad mc;
        for (int i = 0; i < mCiudad.size(); i++) {
            mc = mCiudad.get(i);
            if (mc.getNombre_ciudad() == value) {
                return mc.getId_ciudad();
            }
        }
        return null;
    }

    public Integer getIDStateByText(String value){
        MEstado me;
        for (int i = 0; i < mEstado.size(); i++) {
            me = mEstado.get(i);
            if (me.getNombre_estado() == value) {
                return me.getId_estado();
            }
        }
        return null;
    }

}
