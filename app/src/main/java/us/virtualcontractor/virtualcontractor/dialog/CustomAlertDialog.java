package us.virtualcontractor.virtualcontractor.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import us.virtualcontractor.virtualcontractor.interfaces.EventCustomAlertDialog;

/**
 * Created by ucweb02 on 05/05/2015.
 */
public class CustomAlertDialog {

    private AlertDialog myAlertDialog;
    private AlertDialog.Builder myAlertBuilder;
    private ArrayList<String> myListDialog;
    private String Title = "";
    private String selectedItemsOption = "single";
    private int indexItemSelected = 0;
    private static ArrayList<Integer> indexItemsSelected = new ArrayList<Integer>();
    private EventCustomAlertDialog myEvents;
    private Context ctx;
    private int maxItemsSelected = 0;

    private static boolean[] indexItemsChecked = null;


    public CustomAlertDialog(Context c, ArrayList<String> myListDialog, String title, String selectedoptions) {
        this.myListDialog = myListDialog;
        this.Title = title;
        this.ctx = c;
        this.selectedItemsOption = selectedoptions;
    }

    private void setItemsChecked(){
        if (indexItemsChecked == null) {
            indexItemsChecked = new boolean[myListDialog.size()];
            for (int i = 0; i < indexItemsChecked.length; i++) {
                indexItemsChecked[i] = false;
            }
        }
    }

    private void build(){
        myAlertBuilder = new AlertDialog.Builder(ctx);

        myAlertBuilder.setTitle(Title);

        if (selectedItemsOption == "single") {
            myAlertBuilder.setSingleChoiceItems(myListDialog.toArray(new String[myListDialog.size()]), 0, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int index) {
                indexItemSelected = index;
                myEvents.onADSelectedItem(index);
                }
            });
        } else if (selectedItemsOption == "multiple") {
            myAlertBuilder.setMultiChoiceItems(myListDialog.toArray(new String[myListDialog.size()]), indexItemsChecked,
                new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                        if (isChecked) {
                            // If the user checked the item, add it to the selected items
                            indexItemsSelected.add(which);
                            indexItemsChecked[which] = true;
                            if (indexItemsSelected.size() > maxItemsSelected) {
                                Toast.makeText(ctx, "Max items selected is " + maxItemsSelected, Toast.LENGTH_SHORT).show();
                                indexItemsSelected.remove(indexItemsSelected.size() - 1);
                                indexItemsChecked[which] = false;
                            }
                        } else if (indexItemsSelected.contains(which)) {
                            // Else, if the item is already in the array, remove it
                            indexItemsSelected.remove(Integer.valueOf(which));
                            indexItemsChecked[which] = false;
                        }
                    }
                });
        }

        myAlertBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                if (indexItemsSelected.size() > maxItemsSelected && selectedItemsOption == "multiple") {
                    Toast.makeText(ctx, "Max items selected is " + maxItemsSelected, Toast.LENGTH_SHORT).show();
                } else {
                    myEvents.onADClickButtonOk();
                }
            }
        });
    }

    public void handlerEvents(EventCustomAlertDialog events){
        myEvents = events;
    }

    public void show(){
        setItemsChecked();
        build();
        myAlertDialog = myAlertBuilder.create();
        myAlertDialog.show();
    }

    public void close(){
        myAlertDialog.dismiss();
    }

    public int getIndexItemSelected() {
        return indexItemSelected;
    }

    public ArrayList<Integer> getIndexItemsSelected() {
        return indexItemsSelected;
    }

    public void setMaxItemsSelected(int maxItemsSelected) {
        this.maxItemsSelected = maxItemsSelected;
    }

    public int getMaxItemsSelected() {
        return maxItemsSelected;
    }

    public int getTotalItemsSelected() {
        return indexItemsSelected.size();
    }

    public String getStringItemsSelected(){
        String items = "";
        if (indexItemsSelected.size() <= maxItemsSelected) {
            for (int i = 0; i < indexItemsSelected.size(); i++) {
                items += myListDialog.get(indexItemsSelected.get(i)) + ", ";
            }
            if (items.length() > 0) {
                items = items.substring(0, items.length() - 2);
            }
        }
        return items;
    }

    public String getStringItemSelected(){
        return myListDialog.get(indexItemSelected);
    }

    public boolean[] getIndexItemsChecked() {
        return indexItemsChecked;
    }

    public void setIndexItemsChecked(boolean[] indexItemsChecked) {
        CustomAlertDialog.indexItemsChecked = indexItemsChecked;
    }

}
