package us.virtualcontractor.virtualcontractor.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;

import us.virtualcontractor.virtualcontractor.R;

/**
 * Created by ucweb01 on 28/04/2015.
 */
public class CallContractor extends Dialog implements android.view.View.OnClickListener {

    public Activity c;
    public Dialog d;
    public String number;
    public String email;

    public CallContractor(Activity c,String number,String email) {
        super(c);
        this.c = c;
        this.number = number;
        this.email = email;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.modal_call_contractor);

        this.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        ImageButton call = (ImageButton) findViewById(R.id.call);
        call.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                callPhoneNumber(c,number);
                dismiss();
            }
        });

        ImageButton message = (ImageButton) findViewById(R.id.email);
        message.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                sendEmail(c,email);
                dismiss();
            }
        });
    }

    @Override
    public void onClick(View v) {
        dismiss();
    }

    public static void callPhoneNumber(Activity activity,String phone) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + phone));
        activity.startActivity(callIntent);
    }

    public static void sendEmail(Activity activity, String Address){
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setType("plain/text");
        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{Address});
        activity.startActivity(Intent.createChooser(emailIntent, "Enviar mail"));
    }
}














