package us.virtualcontractor.virtualcontractor.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import us.virtualcontractor.virtualcontractor.R;
import us.virtualcontractor.virtualcontractor.db.consult;
import us.virtualcontractor.virtualcontractor.util.AppPreferences;

/**
 * Created by ucweb01 on 19/05/2015.
 */
public class AddContractor extends Dialog implements android.view.View.OnClickListener {
    private Activity c;
    private String id_contratista;
    private String apeNom;

    private consult oConsult;
    private String msj="";
    private AppPreferences loginData;

    public AddContractor(Activity c,String id_contratista,String apeNom) {
        super(c);
        this.c = c;
        this.id_contratista = id_contratista;
        this.apeNom = apeNom;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.modal_add_contractor);

        this.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        TextView nombreContratista = (TextView) findViewById(R.id.nombreContratista);
        nombreContratista.setText(apeNom);

        ImageButton btn_add = (ImageButton) findViewById(R.id.btn_add);
        btn_add.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //funcion para agregar contratista
                new LoadData().execute();
                //Toast.makeText(getContext(), "Se registro con exito", Toast.LENGTH_SHORT).show();
                dismiss();
            }
        });
    }

    @Override
    public void onClick(View v) {
        dismiss();
    }

    class LoadData extends AsyncTask<String,String,String> {
        @Override
        protected String doInBackground(String... params) {
            oConsult = new consult();
            loginData = new AppPreferences(c,"LoginData");
            msj = oConsult.agregarContratista(loginData.getPreferences("IdUser",""),id_contratista);
            return msj;
        }

        @Override
        protected void onPostExecute(String array){
            super.onPostExecute(array);
            Toast.makeText(getContext(), array, Toast.LENGTH_SHORT).show();
        }
    }
}
