package us.virtualcontractor.virtualcontractor.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import us.virtualcontractor.virtualcontractor.R;
import us.virtualcontractor.virtualcontractor.model.MContratista;
import us.virtualcontractor.virtualcontractor.util.UrlImageViewHelper;

/**
 * Created by ucweb01 on 24/04/2015.
 */
public class AdapterContractors extends ArrayAdapter<MContratista> {
    private final Context context;
    private final ArrayList<MContratista> modelsArrayList;

    public AdapterContractors(Context context, ArrayList<MContratista> modelsArrayList){
        super(context, R.layout.item_contractors, modelsArrayList);
        this.context = context;
        this.modelsArrayList = modelsArrayList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.item_contractors, parent, false);

        TextView apeNom = (TextView) rowView.findViewById(R.id.apeNom);
        apeNom.setText(modelsArrayList.get(position).getNombre() + " " + modelsArrayList.get(position).getApellido());

//        ArrayList<String> profesion = new ArrayList<>();
//        profesion = modelsArrayList.get(position).getProfesion();
//        String all_profesion= (profesion.size() > 0)?profesion.get(0):"";
//        for (int i=1; i < profesion.size(); i++){
//            all_profesion = all_profesion + ", " +profesion.get(i);
//        }
        TextView profesiones = (TextView) rowView.findViewById(R.id.profesiones);
        profesiones.setText(modelsArrayList.get(position).getProfesion());

        TextView numero_email = (TextView) rowView.findViewById(R.id.numero_email);
        numero_email.setVisibility(View.GONE);

        TextView estado_ciudad = (TextView) rowView.findViewById(R.id.estado_ciudad);
        estado_ciudad.setText(modelsArrayList.get(position).getNombre_estado() + " - " + modelsArrayList.get(position).getNombre_ciudad());


        ImageView img = (ImageView) rowView.findViewById(R.id.img);
        UrlImageViewHelper.setUrlDrawable(img, "http://virtualc.uc-web.mobi/web_service/fotos_persona/" + modelsArrayList.get(position).getAvatar());

        return rowView;
    }
}
