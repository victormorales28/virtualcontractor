package us.virtualcontractor.virtualcontractor.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import us.virtualcontractor.virtualcontractor.R;
import us.virtualcontractor.virtualcontractor.model.MWeek;

import java.util.ArrayList;

/**
 * Created by ucweb01 on 27/04/2015.
 */
public class AdapterWeek extends ArrayAdapter<MWeek> {
    private final Context context;
    private final ArrayList<MWeek> modelsArrayList;

    public AdapterWeek(Context context, ArrayList<MWeek> modelsArrayList){
        super(context, R.layout.item_week, modelsArrayList);
        this.context = context;
        this.modelsArrayList = modelsArrayList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView;
        /*if(modelsArrayList.get(position).getHeader()==1){
            rowView = inflater.inflate(R.layout.header_detail_search_project, parent, false);
        }
        else{*/
            rowView = inflater.inflate(R.layout.item_week, parent, false);

            TextView txtTitle = (TextView) rowView.findViewById(R.id.nameWeek);
            txtTitle.setText(modelsArrayList.get(position).getNombre());
        //}
        return rowView;
    }
}












