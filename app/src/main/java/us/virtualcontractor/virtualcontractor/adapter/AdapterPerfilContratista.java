package us.virtualcontractor.virtualcontractor.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import us.virtualcontractor.virtualcontractor.R;
import us.virtualcontractor.virtualcontractor.model.MPerfilContratista;
import us.virtualcontractor.virtualcontractor.util.UrlImageViewHelper;

/**
 * Created by ucweb01 on 19/05/2015.
 */
public class AdapterPerfilContratista extends ArrayAdapter<MPerfilContratista> {
    private final Context context;
    private final ArrayList<MPerfilContratista> modelsArrayList;
    private int head=0;

    public AdapterPerfilContratista(Context context,ArrayList<MPerfilContratista> modelsArrayList){
        super(context, R.layout.item_project, modelsArrayList);
        this.context = context;
        this.modelsArrayList = modelsArrayList;
    }

    public AdapterPerfilContratista(Context context,ArrayList<MPerfilContratista> modelsArrayList, int head){
        super(context, R.layout.item_project, modelsArrayList);
        this.context = context;
        this.modelsArrayList = modelsArrayList;
        this.head = head;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView;
        if(position != 0 || head == 1){
            rowView = inflater.inflate(R.layout.item_project, parent, false);

            ImageView imgProyecto = (ImageView) rowView.findViewById(R.id.imgProyecto);
            UrlImageViewHelper.setUrlDrawable(imgProyecto, "http://virtualc.uc-web.mobi/web_service/fotos_semana/" + modelsArrayList.get(position).getFoto_proyecto());

            TextView nombreProyecto = (TextView) rowView.findViewById(R.id.nombreProyecto);
            nombreProyecto.setText(modelsArrayList.get(position).getNombre_proyecto());

            TextView sub_titulo = (TextView) rowView.findViewById(R.id.sub_titulo);
            sub_titulo.setText(modelsArrayList.get(position).getSubTitulo());

            int[] star = new int[]{R.id.star1,R.id.star2,R.id.star3,R.id.star4,R.id.star5};

            for (int i= 0; i < Math.round(modelsArrayList.get(position).getValoracion()); i++){
                ImageView img= (ImageView) rowView.findViewById(star[i]);
                img.setImageResource(R.drawable.star_on);
            }
        }
        else{
            rowView = inflater.inflate(R.layout.head_perfil_contratista, parent, false);

            TextView apeNom = (TextView) rowView.findViewById(R.id.apeNom);
            apeNom.setText(modelsArrayList.get(position).getNombre() + " " + modelsArrayList.get(position).getApellido());

            ImageView imgContratista = (ImageView) rowView.findViewById(R.id.imgContratista);
            UrlImageViewHelper.setUrlDrawable(imgContratista, "http://virtualc.uc-web.mobi/web_service/fotos_persona/" + modelsArrayList.get(position).getAvatar());

            TextView profesion = (TextView) rowView.findViewById(R.id.profesion);
            profesion.setText(modelsArrayList.get(position).getProfesion());

            TextView numero_email = (TextView) rowView.findViewById(R.id.numero_email);
            String numero = (modelsArrayList.get(position).getMovil() != "")?modelsArrayList.get(position).getMovil():((modelsArrayList.get(position).getTelefono()!="")?modelsArrayList.get(position).getTelefono():"");
            numero_email.setText(numero + " | " + modelsArrayList.get(position).getEmail());

            TextView estado_ciudad = (TextView) rowView.findViewById(R.id.estado_ciudad);
            estado_ciudad.setText(modelsArrayList.get(position).getNombre_estado() + " - " + modelsArrayList.get(position).getNombre_ciudad());

            if (modelsArrayList.size() == 1){
                TextView myProyect = (TextView) rowView.findViewById(R.id.myProyect);
                myProyect.setText("There is not projects");
            }
        }
        return rowView;
    }
}
