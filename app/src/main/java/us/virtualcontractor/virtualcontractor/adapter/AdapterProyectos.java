package us.virtualcontractor.virtualcontractor.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import us.virtualcontractor.virtualcontractor.R;
import us.virtualcontractor.virtualcontractor.model.MProyectos;

/**
 * Created by ucweb01 on 16/04/2015.
 */
public class AdapterProyectos extends ArrayAdapter<MProyectos> {
    private final Context context;
    private final ArrayList<MProyectos> modelsArrayList;

    public AdapterProyectos(Context context,ArrayList<MProyectos> modelsArrayList){
        super(context, R.layout.item_project, modelsArrayList);
        this.context = context;
        this.modelsArrayList = modelsArrayList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.item_project, parent, false);

        TextView txtTitle = (TextView) rowView.findViewById(R.id.nombreProyecto);
        txtTitle.setText("proy");//modelsArrayList.get(position).getNombre()

        return rowView;
    }
}




