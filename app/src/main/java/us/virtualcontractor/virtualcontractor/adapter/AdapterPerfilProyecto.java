package us.virtualcontractor.virtualcontractor.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import us.virtualcontractor.virtualcontractor.R;
import us.virtualcontractor.virtualcontractor.model.MPerfilProyecto;

/**
 * Created by ucweb01 on 20/05/2015.
 */
public class AdapterPerfilProyecto extends ArrayAdapter<MPerfilProyecto>{
    private final Context context;
    private final ArrayList<MPerfilProyecto> modelsArrayList;

    public AdapterPerfilProyecto(Context context,ArrayList<MPerfilProyecto> modelsArrayList){
        super(context, R.layout.item_week, modelsArrayList);
        this.context = context;
        this.modelsArrayList = modelsArrayList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView;
        if(position != 0){
            rowView = inflater.inflate(R.layout.item_week, parent, false);

            TextView nameWeek = (TextView) rowView.findViewById(R.id.nameWeek);
            nameWeek.setText("WEEK "+ modelsArrayList.get(position).getNumero_semana());
        }
        else{
            rowView = inflater.inflate(R.layout.head_perfil_proyecto, parent, false);

            TextView nombreProyecto = (TextView) rowView.findViewById(R.id.nombreProyecto);
            nombreProyecto.setText(modelsArrayList.get(position).getNombre_proyecto());

            TextView subTitulo = (TextView) rowView.findViewById(R.id.subTitulo);
            subTitulo.setText(modelsArrayList.get(position).getSub_titulo());

            TextView descripcion = (TextView) rowView.findViewById(R.id.descripcion);
            descripcion.setText(modelsArrayList.get(position).getDescripcion());

            TextView totalSemanas = (TextView) rowView.findViewById(R.id.totalSemanas);
            totalSemanas.setText(String.valueOf(modelsArrayList.get(position).getTotal_semanas())+ " WEEK");

            TextView fechaInicio = (TextView) rowView.findViewById(R.id.fechaInicio);
            fechaInicio.setText(modelsArrayList.get(position).getFecha_inicio());
        }
        return rowView;
    }
}
