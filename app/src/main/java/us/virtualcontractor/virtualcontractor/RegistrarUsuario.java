package us.virtualcontractor.virtualcontractor;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import us.virtualcontractor.virtualcontractor.db.ConnectServer;
import us.virtualcontractor.virtualcontractor.db.consult;
import us.virtualcontractor.virtualcontractor.dialog.CustomAlertDialog;
import us.virtualcontractor.virtualcontractor.interfaces.EventCustomAlertDialog;
import us.virtualcontractor.virtualcontractor.interfaces.ServerConnectionListener;
import us.virtualcontractor.virtualcontractor.interfaces.TaskImageListener;
import us.virtualcontractor.virtualcontractor.model.MLocation;
import us.virtualcontractor.virtualcontractor.model.MProfesiones;
import us.virtualcontractor.virtualcontractor.util.AsyncTaskImage;
import us.virtualcontractor.virtualcontractor.util.IDViews;


public class RegistrarUsuario extends ActionBarActivity implements EventCustomAlertDialog, TaskImageListener, ServerConnectionListener {

    private EditText txtRegistrarComo;
    private EditText txtCompany;
    private EditText txtState;
    private EditText txtCity;
    private EditText txtProfession;
    private EditText txtEmail;
    private Button btnSignIn;
    private Button btnUploadAvatar;
    private TextView btnTerms;
    private CheckBox chkTerms;
    private AlertDialog myAlertDialog;
    private AlertDialog.Builder myAlertBuilder;
    private static int alertSelectedItem = 0;
    private static int alertSelectedItemStates = 0;
    private ListAlertDialog objListAlertDialog = new ListAlertDialog();
    private ArrayList<String> listAlertDialog;
    private EmailValidator emailValidator;

    private ArrayList<String> listaEstados = null;
    private ArrayList<String> listaCiudades = null;
    private consult oConsult;
    private MLocation patternLocation;

    private CustomAlertDialog ctmAlertDialog;
    private int indexShowDialog = 0;

    private ArrayList<String> listaProfesiones = null;
    private MProfesiones objProfesiones = null;

    private ArrayList<String> encodeImageString = new ArrayList<String>();
    private String pathImage = "";
    private AsyncTaskImage taskImage;
    private ConnectServer cnServer;

    private static int RESULT_LOAD_CAMERA = 1;
    private static int RESULT_LOAD_GALLERY = 2;

    private Uri uriAvatar;

    private ProgressDialog progressDialog;

    private String[] idsItemsSelected = new String[4];

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_user);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.title_SignIn));

        new LoadData().execute();

        txtRegistrarComo = (EditText) findViewById(R.id.txtRegisterAs);
        txtCompany = (EditText) findViewById(R.id.txtCompany);
        txtState = (EditText) findViewById(R.id.txtState);
        txtCity = (EditText) findViewById(R.id.txtCity);
        txtProfession = (EditText) findViewById(R.id.txtProfession);
        txtEmail = (EditText) findViewById(R.id.txtEmail);
        btnSignIn = (Button) findViewById(R.id.btnSignin);
        btnUploadAvatar = (Button) findViewById(R.id.btnUploadAvatar);
        btnTerms = (TextView) findViewById(R.id.btnTerms);
        chkTerms = (CheckBox) findViewById(R.id.chkTerms);

        myAlertBuilder = new AlertDialog.Builder(RegistrarUsuario.this);

        txtRegistrarComo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ctmAlertDialog = new CustomAlertDialog(RegistrarUsuario.this, objListAlertDialog.listaRegistrarComo, "Select type user.", "single");
                ctmAlertDialog.handlerEvents(RegistrarUsuario.this);
                ctmAlertDialog.show();
                indexShowDialog = 1;
            }
        });

        txtState.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            if (listaEstados != null) {
                ctmAlertDialog = new CustomAlertDialog(RegistrarUsuario.this, listaEstados, "Select State.", "single");
                ctmAlertDialog.handlerEvents(RegistrarUsuario.this);
                ctmAlertDialog.show();
                indexShowDialog = 2;
                txtCity.setText("");
            }
            }
        });

        txtCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            if (String.valueOf(txtState.getText()).isEmpty()) {
                Toast.makeText(getApplicationContext(), "Must select a state.", Toast.LENGTH_SHORT).show();
            } else {
                ctmAlertDialog = new CustomAlertDialog(RegistrarUsuario.this, listaCiudades, "Select City.", "single");
                ctmAlertDialog.handlerEvents(RegistrarUsuario.this);
                ctmAlertDialog.show();
                indexShowDialog = 3;
            }
            }
        });

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateFields();
            }
        });

        txtProfession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            txtProfession.setText("");
            if (listaProfesiones != null) {
                ctmAlertDialog = new CustomAlertDialog(RegistrarUsuario.this, listaProfesiones, "Select profession.", "multiple");
                ctmAlertDialog.handlerEvents(RegistrarUsuario.this);
                ctmAlertDialog.setMaxItemsSelected(5);
                ctmAlertDialog.show();
                indexShowDialog = 4;
            }
            }
        });

        btnTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            CustomRegisterModal dialog = new CustomRegisterModal(RegistrarUsuario.this, 0);
            dialog.show();
            }
        });

        btnUploadAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            ArrayList<String> lista = new ArrayList<String>(Arrays.asList("Camera", "Gallery"));
            ctmAlertDialog = new CustomAlertDialog(RegistrarUsuario.this, lista, "Select source.", "single");
            ctmAlertDialog.handlerEvents(RegistrarUsuario.this);
            ctmAlertDialog.show();
            indexShowDialog = 5;
            }
        });

        emailValidator = new EmailValidator();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
			// When an Image is picked
			if (requestCode == RESULT_LOAD_GALLERY && resultCode == RESULT_OK && null != data) { // GALERIA
				// Get the Image from data

                uriAvatar = data.getData();
				String[] filePathColumn = { MediaStore.Images.Media.DATA };

				// Get the cursor
				Cursor cursor = getContentResolver().query(uriAvatar, filePathColumn, null, null, null);
				// Move to first row
				cursor.moveToFirst();

				int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                pathImage = cursor.getString(columnIndex);
				cursor.close();
            }

            if (requestCode == RESULT_LOAD_GALLERY || requestCode == RESULT_LOAD_CAMERA) {
                if (resultCode == RESULT_OK) {
                    Toast.makeText(this, "Correctly selected image.", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(this, "Image selection canceled.", Toast.LENGTH_LONG).show();
                }
            }

//          showDialogImagePreview();
		} catch (Exception e) {
			Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
		}
    }

    private void showDialogImagePreview(){
//        Dialog dlgImageFull = new Dialog(RegistrarUsuario.this);
//        dlgImageFull.setContentView(R.layout.modal_full_image);
//        ImageView img = (ImageView) dlgImageFull.findViewById(R.id.imgFullPreview);
//        img.setImageURI(uriAvatar);
//        dlgImageFull.show();
        ImageFullPreview modalImage = new ImageFullPreview(RegistrarUsuario.this, 0, pathImage);
        modalImage.show();
    }

    @Override
    public void onADSelectedItem(int index) {

    }

    @Override
    public void onADClickButtonOk() {
        switch (indexShowDialog){
            case 1:
                if (ctmAlertDialog.getIndexItemSelected() == 0) {
                    txtCompany.setVisibility(View.VISIBLE);
                    txtProfession.setVisibility(View.VISIBLE);
                    idsItemsSelected[0] = "1";
                } else {
                    txtCompany.setVisibility(View.GONE);
                    txtProfession.setVisibility(View.GONE);
                    idsItemsSelected[0] = "2";
                }
                txtRegistrarComo.setText(ctmAlertDialog.getStringItemSelected());
                break;
            case 2:
                txtState.setText(ctmAlertDialog.getStringItemSelected());
                idsItemsSelected[1] = String.valueOf(patternLocation.getIDStateByText(ctmAlertDialog.getStringItemSelected()));
                listaCiudades = patternLocation.getListaCiudadPorEstado(ctmAlertDialog.getIndexItemSelected());
                break;
            case 3:
                txtCity.setText(ctmAlertDialog.getStringItemSelected());
                idsItemsSelected[2] = String.valueOf(patternLocation.getIDCityByText(ctmAlertDialog.getStringItemSelected()));
                break;
            case 4:
                txtProfession.setText(ctmAlertDialog.getStringItemsSelected());
                idsItemsSelected[3] = objProfesiones.getIDSProfesion(ctmAlertDialog.getIndexItemsSelected());
                break;
            case 5:
                if (ctmAlertDialog.getIndexItemSelected() == 0) {
            		File imagesFolder = new File(Environment.getExternalStorageDirectory(), "/Virtual Contractor/Media/Upload");
            		if (!imagesFolder.exists()) {
            			imagesFolder.mkdirs();
            		}
                    pathImage = Environment.getExternalStorageDirectory() + "/Virtual Contractor/Media/Upload/avatar.jpg";
                    File file = new File(pathImage);
            		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    uriAvatar = Uri.fromFile(file);
            		intent.putExtra(MediaStore.EXTRA_OUTPUT, uriAvatar);
            		startActivityForResult(intent, RESULT_LOAD_CAMERA);
                } else {
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(galleryIntent, RESULT_LOAD_GALLERY);
                }
                break;
        }
    }

    @Override
    public void onAsyncTaskImageFinished(ArrayList<String> encodeImages) {
        if (encodeImages.size() > 0) {
            cnServer = new ConnectServer(RegistrarUsuario.this, "register.php");
            cnServer.handleEvents(RegistrarUsuario.this);
            cnServer.setProgressDialog(progressDialog);
            if (idsItemsSelected[0] == "1") {
                loadParamsContractorHttpPost();
            } else {
                loadParamsClientHttpPost();
            }
            cnServer.addParameter("image", encodeImages.get(0));
            cnServer.Upload();
        } else {
            Toast.makeText(this, "Error encode images, null data.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void serverConnectionOnSuccess(String response) {
        try {
            JSONObject json = new JSONObject(response);
            JSONArray data = json.optJSONArray("register");
            JSONObject obj = data.getJSONObject(0);
            if (obj.optInt("message") == 0) {
                Toast.makeText(RegistrarUsuario.this, "Sorry, it seems that " + txtEmail.getText().toString() +  " belongs to an existing account.", Toast.LENGTH_SHORT).show();
            } else if (obj.optInt("message") == 1) {
                AlertDialog dlgRegister;
                AlertDialog.Builder dlgBRegister = new AlertDialog.Builder(RegistrarUsuario.this);
                dlgBRegister.setTitle("Sign In");
                dlgBRegister.setMessage("Registered properly.");
                dlgBRegister.setCancelable(false);
                dlgBRegister.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        goBack();
                    }
                });
                dlgRegister = dlgBRegister.create();
                dlgRegister.show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void loadParamsClientHttpPost(){
        cnServer.addParameter("tipo_usuario", idsItemsSelected[0]);
        cnServer.addParameter("nombres", getTextByID(IDViews.ID_VALIDATE_CLIENT[0]));
        cnServer.addParameter("apellidos", getTextByID(IDViews.ID_VALIDATE_CLIENT[1]));
        cnServer.addParameter("telefono", getTextByID(IDViews.ID_VALIDATE_CLIENT[2]));
        cnServer.addParameter("movil", getTextByID(IDViews.ID_VALIDATE_CLIENT[3]));
        cnServer.addParameter("id_ciudad", idsItemsSelected[2]);
        cnServer.addParameter("email", getTextByID(IDViews.ID_VALIDATE_CLIENT[6]));
        cnServer.addParameter("contrasenia", getTextByID(IDViews.ID_VALIDATE_CLIENT[7]));
    }

    private void loadParamsContractorHttpPost(){
        cnServer.addParameter("tipo_usuario", idsItemsSelected[0]);
        cnServer.addParameter("nombres", getTextByID(IDViews.ID_VALIDATE_CONTRACTOR[0]));
        cnServer.addParameter("apellidos", getTextByID(IDViews.ID_VALIDATE_CONTRACTOR[1]));
        cnServer.addParameter("compania", getTextByID(IDViews.ID_VALIDATE_CONTRACTOR[2]));
        cnServer.addParameter("telefono", getTextByID(IDViews.ID_VALIDATE_CONTRACTOR[3]));
        cnServer.addParameter("movil", getTextByID(IDViews.ID_VALIDATE_CONTRACTOR[4]));
        cnServer.addParameter("id_ciudad", idsItemsSelected[2]);
        cnServer.addParameter("id_profesion", idsItemsSelected[3]);
        cnServer.addParameter("email", getTextByID(IDViews.ID_VALIDATE_CONTRACTOR[8]));
        cnServer.addParameter("contrasenia", getTextByID(IDViews.ID_VALIDATE_CONTRACTOR[9]));
    }

    private String getTextByID(int id){
        EditText txt = (EditText) findViewById(id);
        return txt.getText().toString().trim();
    }

    class LoadData extends AsyncTask<String,String,String> {
        @Override
        protected String doInBackground(String... params) {
            oConsult = new consult();
            patternLocation = oConsult.getEstadoCiudad();
            listaEstados = patternLocation.getListaEstados();
            objProfesiones = oConsult.getProfesiones();
            listaProfesiones = objProfesiones.getListaProfesiones();
            return null;
        }
    }

    public Boolean validateFields() {
        Boolean validarMensajeError = false;
        Boolean validarDatosUsuario = false;
        String mensaje = "", titulo = "Warning: Fields incorrect.";
        String contrasenia = "";
        String contraseniaConfirmar = "";

        if (idsItemsSelected[0] == "1") {
            validarDatosUsuario = validateIsEmptyInputs(IDViews.ID_VALIDATE_CONTRACTOR, -1, IDViews.MSG_ERROR_VALIDATE_CONTRACTOR);
            contrasenia = String.valueOf(getTextByID(IDViews.ID_VALIDATE_CONTRACTOR[9]));
            contraseniaConfirmar = String.valueOf(getTextByID(IDViews.ID_VALIDATE_CONTRACTOR[10]));
        } else if (idsItemsSelected[0] == "2") {
            validarDatosUsuario = validateIsEmptyInputs(IDViews.ID_VALIDATE_CLIENT, -1, IDViews.MSG_ERROR_VALIDATE_CLIENT);
            contrasenia = String.valueOf(getTextByID(IDViews.ID_VALIDATE_CLIENT[7]));
            contraseniaConfirmar = String.valueOf(getTextByID(IDViews.ID_VALIDATE_CLIENT[8]));
        }

        if (txtRegistrarComo.getText().toString().isEmpty()) {
            mensaje = "Type User is required.";
        } else {
            if (!validarDatosUsuario) {
                if (emailValidator.validate(String.valueOf(txtEmail.getText()))) {
                    if (contrasenia.equals(contraseniaConfirmar)) {
                        if (pathImage != "") {
                            if (chkTerms.isChecked()) {
                                validarMensajeError = true;
                                taskImage = new AsyncTaskImage(RegistrarUsuario.this);
                                ArrayList<String> imgPath = new ArrayList<String>(Arrays.asList(pathImage));
                                progressDialog = new ProgressDialog(this);
                                progressDialog.setCancelable(false);
                                progressDialog.show();
                                taskImage.setProgressDialog(progressDialog);
                                taskImage.execute(imgPath);
                            } else {
                                mensaje = "You must accept the terms of use.";
                            }
                        } else {
                            mensaje = "Select your avatar.";
                        }
                    } else {
                        mensaje += "The passwords entered do not match.";
                    }
                } else {
                    if (contrasenia.equals(contraseniaConfirmar)) {
                        mensaje += "The entered email is not valid.";
                    } else {
                        mensaje += "The entered email is not valid.\n\n";
                        mensaje += "The passwords entered do not match.";
                    }
                }
            }
        }

        if (!validarMensajeError && !mensaje.isEmpty()) {
            AlertDialog.Builder ab = new AlertDialog.Builder(RegistrarUsuario.this);
            ab.setTitle(titulo)
            .setMessage(mensaje)
            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    myAlertDialog.dismiss();
                }
            });
            myAlertDialog = ab.create();
            myAlertDialog.show();
        }

        return validarMensajeError;
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_registrar_usuario, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        switch (item.getItemId()) {
//            // Respond to the action bar's Up/Home button
//            case android.R.id.home:
////                NavUtils.navigateUpFromSameTask(this);
//                goBack();
//                return true;
//            case R.id.action_settings:
//                return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }

    @Override
    public void onBackPressed() {
//        moveTaskToBack(false);
        goBack();
//        ObjectAnimator animX = ObjectAnimator.ofFloat(this, "x", 50f);
//        ObjectAnimator animY = ObjectAnimator.ofFloat(this, "y", 100f);
//        AnimatorSet animSetXY = new AnimatorSet();
//        animSetXY.playTogether(animX, animY);
//        animSetXY.start();
    }

    private void goBack(){
        finish();
        overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
    }

    private Boolean validateIsEmptyInputs(int[] idsValidate, int validateInput, String[] msgError) {
        Boolean validate = false;
        EditText txt;
        for (int i = 0; i < idsValidate.length; i++) {
            if (validateInput != -1 && validateInput != idsValidate[i]) continue;
            txt = (EditText) findViewById(idsValidate[i]);
            if (txt.getText().toString().trim().length() == 0) {
                txt.setError(msgError[i]);
                validate = true;
            } else {
                txt.setError(null);
            }
        }
        return validate;
    }

}